package io.bifri.productcatalog.util

import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.core.content.getSystemService


fun TextView.setTextIfChanged(newText: CharSequence?) {
    if (isTextChanged(newText)) text = newText
}

fun TextView.isTextChanged(newText: CharSequence?) =
        text?.toString() ?: "" != newText?.toString() ?: ""

fun View.hideKeyboard() {
    val imm: InputMethodManager = context.getSystemService() ?: return
    imm.hideSoftInputFromWindow(windowToken, 0)
}