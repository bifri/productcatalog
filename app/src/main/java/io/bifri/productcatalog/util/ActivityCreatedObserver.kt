package io.bifri.productcatalog.util

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner


class ActivityCreatedObserver(
    private val action: () -> Unit
) : DefaultLifecycleObserver {

    override fun onCreate(owner: LifecycleOwner) {
        owner.lifecycle.removeObserver(this)
        action()
    }

}