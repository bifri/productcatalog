package io.bifri.productcatalog.network.response.categories

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

data class Category @JsonCreator constructor(
    @JsonProperty("category_id") val categoryId: String?, // 4465
    @JsonProperty("featured") val featured: String?, // 0
    @JsonProperty("full_name") val fullName: String?, // ATV Box Accessories
    @JsonProperty("orderable_count") val orderableCount: String?, // 133
    @JsonProperty("parent_id") val parentId: String?, // 4414
    @JsonProperty("popularity") val popularity: String?, // 96.905
    @JsonProperty("short_name") val shortName: String?, // ATV Box Accessories
    @JsonProperty("unavailable_count") val unavailableCount: String?, // 160
    @JsonProperty("url") val url: String? // atv-boxes-accessories
) {
    val categoryIdNonNull get() = categoryId!!

    val shortNameNonNull get() = shortName.orEmpty()
}