package io.bifri.productcatalog.network.productsservice

import retrofit2.Retrofit


class ProductsApi(retrofit: Retrofit) {

    private val service: ProductsService = retrofit.create(ProductsService::class.java)

    suspend fun categories() = service.categories()

    suspend fun products(id: String) = service.products(id)

    suspend fun product(id: String) = service.product(id)

}