package io.bifri.productcatalog.network.processor

import io.bifri.productcatalog.exception.ApiException
import io.bifri.productcatalog.network.productsservice.ProductsApi
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.*

class SimpleProcessor(
    private val apiExceptionProcessor: ApiExceptionProcessor,
    private val productsApi: ProductsApi
) {

    suspend fun <T : Any> load(
        apiCall: suspend ProductsApi.() -> T
    ): Flow<Result<T>> = flow { emit(productsApi.apiCall()) }
        .map {
            @Suppress("USELESS_CAST")
            Success(it) as Result<T>
        }
        .onStart { emit(Loading()) }
        .catch {
            apiExceptionProcessor.processApiException(it)
            emit(Error(ApiException(it)))
        }
        .flowOn(IO)

}