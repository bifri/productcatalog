package io.bifri.productcatalog.network.response.categories

val List<Category?>.sanitized: List<Category> get() =
    filterNotNull().filter { !it.categoryId.isNullOrEmpty()}