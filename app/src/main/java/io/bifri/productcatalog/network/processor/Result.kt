package io.bifri.productcatalog.network.processor

import io.bifri.productcatalog.exception.ApiException

sealed class Result<out T>

class Loading<T> : Result<T>()

class Error<T>(val error: ApiException) : Result<T>()

class Success<T>(val value: T) : Result<T>()