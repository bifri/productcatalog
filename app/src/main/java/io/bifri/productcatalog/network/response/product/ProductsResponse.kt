package io.bifri.productcatalog.network.response.product

import io.bifri.productcatalog.network.response.products.ProductsResponse
import io.bifri.productcatalog.network.response.products.ProductsResponse.GridProducts.Element

val ProductsResponse.sanitizedProducts: List<Element> get() =
    gridProducts?.elements?.filterNotNull()?.filter { it.productId != null }.orEmpty()