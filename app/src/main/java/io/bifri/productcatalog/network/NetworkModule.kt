package io.bifri.productcatalog.network

import dagger.Module
import io.bifri.productcatalog.network.productsservice.ProductsServiceModule


@Module(includes = [ProductsServiceModule::class])
class NetworkModule