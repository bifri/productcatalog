package io.bifri.productcatalog.network.response.products
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal


data class ProductsResponse @JsonCreator constructor(
//        @JsonProperty("directDeals") val directDeals: List<Any?>?,
        @JsonProperty("enablePriceRange") val enablePriceRange: Boolean?, // false
        @JsonProperty("enablePriceRangeSlider") val enablePriceRangeSlider: Boolean?, // false
        @JsonProperty("enableRatings") val enableRatings: Boolean?, // false
        @JsonProperty("filtered") val filtered: Int?, // 7
        @JsonProperty("filtered_variants") val filteredVariants: Int?, // 7
        @JsonProperty("grid_config") val gridConfig: GridConfig?,
        @JsonProperty("gridProducts") val gridProducts: GridProducts?,
        @JsonProperty("grid_size") val gridSize: Int?, // 60
        @JsonProperty("gridType") val gridType: String?, // product
        @JsonProperty("listPageGridType") val listPageGridType: String?, // combined
        @JsonProperty("showPriceRange") val showPriceRange: Boolean?, // false
        @JsonProperty("showPriceRangeSlider") val showPriceRangeSlider: Boolean?, // true
        @JsonProperty("showRatings") val showRatings: Boolean?, // false
        @JsonProperty("total") val total: Int?, // 7
        @JsonProperty("total_variants") val totalVariants: Int? // 7
) {

    data class GridConfig @JsonCreator constructor(
            @JsonProperty("size") val size: Int? // 60
    )

    data class GridProducts @JsonCreator constructor(
            @JsonProperty("elements") val elements: List<Element?>?,
            @JsonProperty("headingName") val headingName: String?,
            @JsonProperty("url") val url: String?
    ) {
        data class Element @JsonCreator constructor(
                @JsonProperty("allVariantsAreOutlet") val allVariantsAreOutlet: Boolean?, // false
                @JsonProperty("allVariantsAreShed") val allVariantsAreShed: Boolean?, // false
                @JsonProperty("anchorText") val anchorText: String?, // Mad Dog ATV Comfort Ride Seat Protector
                @JsonProperty("brand") val brand: Brand?,
//                @JsonProperty("brandCategories") val brandCategories: List<Any?>?,
                @JsonProperty("brandName") val brandName: String?, // MadDog
//                @JsonProperty("bundleData") val bundleData: List<Any?>?,
                @JsonProperty("categories") val categories: List<Category?>?,
                @JsonProperty("categoryName") val categoryName: String?, // ATV Box Accessories / ATV Boxes / Auto &amp; Atv
                @JsonProperty("clearanceVariantCount") val clearanceVariantCount: Int?, // 0
                @JsonProperty("deals") val deals: List<Deal?>?,
                @JsonProperty("elementDimensions") val elementDimensions: ElementDimensions?,
                @JsonProperty("fullName") val fullName: String?, // Mad Dog ATV Comfort Ride Seat Protector
                @JsonProperty("gridItemName") val gridItemName: String?, // Mad Dog ATV Comfort Ride Seat Protector
                @JsonProperty("gridName") val gridName: String?, // Category Products
                @JsonProperty("hasOptions") val hasOptions: Int?, // 0
                @JsonProperty("hasVideo") val hasVideo: Boolean?, // false
//                @JsonProperty("highlightedDescription") val highlightedDescription: List<Any?>?,
                @JsonProperty("id") val id: Long?, // 773046
                @JsonProperty("imageHeight") val imageHeight: String?, // 185
                @JsonProperty("imageWidth") val imageWidth: String?, // 196
                @JsonProperty("isCallToOrder") val isCallToOrder: Int?, // 0
                @JsonProperty("isMadeInUsa") val isMadeInUsa: Int?, // 0
                @JsonProperty("isOrderable") val isOrderable: Boolean?, // true
                @JsonProperty("isOutlet") val isOutlet: Boolean?, // false
                @JsonProperty("isPriceAfterRebate") val isPriceAfterRebate: Boolean?, // false
                @JsonProperty("isSaveExact") val isSaveExact: Boolean?, // true
                @JsonProperty("isSavingExact") val isSavingExact: Boolean?, // true
                @JsonProperty("isSegmentSalePriceHidden") val isSegmentSalePriceHidden: Boolean?, // false
                @JsonProperty("isShed") val isShed: Boolean?, // false
                @JsonProperty("itemFlag") val itemFlag: String?, // sale
                @JsonProperty("killerDealVariantCount") val killerDealVariantCount: Int?, // 0
                @JsonProperty("listPrice") val listPrice: BigDecimal?, // 23.99
                @JsonProperty("mainCategory") val mainCategory: MainCategory?,
                @JsonProperty("matchedVariantCount") val matchedVariantCount: Int?, // 1
                @JsonProperty("popularity") val popularity: BigDecimal?, // 46.168
                @JsonProperty("price") val price: BigDecimal?, // 21.99
                @JsonProperty("priceFlag") val priceFlag: String?, // On Sale
                @JsonProperty("primaryImage") val primaryImage: String?, // opplanet-mad-dog-atv-comfort-ride-seat-protector-2000012623-main
                @JsonProperty("productCode") val productCode: String?, // 2GZ-AA-2000012623
                @JsonProperty("productId") val productId: Long?, // 773046
//                @JsonProperty("promotion") val promotion: List<Any?>?,
                @JsonProperty("reviewCount") val reviewCount: Int?, // 0
                @JsonProperty("reviewRating") val reviewRating: String?, // 0
                @JsonProperty("ribbonPriority") val ribbonPriority: String?, // medium-priority
                @JsonProperty("saveAmount") val saveAmount: Int?, // 2
                @JsonProperty("savePercent") val savePercent: Int?, // 8
                @JsonProperty("savingsText") val savingsText: String?, // Save $2.00
                @JsonProperty("shortName") val shortName: String?, // ATV Comfort Ride Seat Protector
                @JsonProperty("showAsLowAs") val showAsLowAs: Boolean?, // false
                @JsonProperty("specialOffers") val specialOffers: SpecialOffers?,
//                @JsonProperty("stores") val stores: List<Any?>?,
//                @JsonProperty("topSpecifications") val topSpecifications: List<Any?>?,
                @JsonProperty("url") val url: String?, // mad-dog-atv-comfort-ride-seat-protector
                @JsonProperty("variantCount") val variantCount: Int?, // 1
                @JsonProperty("variantCountText") val variantCountText: Int? // 1
        ) {

            val productIdNonNull get() = productId!!

            val shortNameNonNull get() = shortName.orEmpty()

            data class Brand @JsonCreator constructor(
                    @JsonProperty("name") val name: String? // MadDog
            )

            data class Category @JsonCreator constructor(
                    @JsonProperty("lev") val lev: String?, // 3
                    @JsonProperty("name") val name: String?, // ATV Box Accessories
                    @JsonProperty("popularity") val popularity: String?, // 78.607
                    @JsonProperty("url") val url: String? // atv-boxes-accessories
            )

            data class Deal @JsonCreator constructor(
                    @JsonProperty("badgeColor") val badgeColor: BadgeColor?,
                    @JsonProperty("badgeName") val badgeName: String?, // On Sale
                    @JsonProperty("badgePopupText") val badgePopupText: String?,
                    @JsonProperty("description") val description: String?, // This item is currently on sale and has a new, lower price! %anchorTag%
                    @JsonProperty("header") val header: String?, // On Sale
                    @JsonProperty("parameters") val parameters: Parameters?,
                    @JsonProperty("promoType") val promoType: String?, // On Sale
                    @JsonProperty("showBadge") val showBadge: Boolean?, // true
                    @JsonProperty("tabName") val tabName: String?, // On Sale
                    @JsonProperty("type") val type: String?, // sale
                    @JsonProperty("typeSlug") val typeSlug: String? // on-sale
            ) {
                data class BadgeColor @JsonCreator constructor(
                        @JsonProperty("b") val b: Int?, // 212
                        @JsonProperty("g") val g: Int?, // 143
                        @JsonProperty("hex") val hex: String?, // #538FD4
                        @JsonProperty("r") val r: Int? // 83
                )

                data class Parameters @JsonCreator constructor(
                        @JsonProperty("%anchorTag%") val anchorTag: AnchorTag?
                ) {
                    data class AnchorTag @JsonCreator constructor(
                            @JsonProperty("absolute") val absolute: Boolean?, // false
                            @JsonProperty("attributes") val attributes: Attributes?,
                            @JsonProperty("text") val text: String?, // See more items on sale
                            @JsonProperty("type") val type: String?, // link
                            @JsonProperty("url") val url: String? // specials-closeouts
                    ) {
                        class Attributes @JsonCreator constructor(
                        )
                    }
                }
            }

            data class ElementDimensions @JsonCreator constructor(
                    @JsonProperty("height") val height: Int?, // 2
                    @JsonProperty("width") val width: Int? // 1
            )

            data class MainCategory @JsonCreator constructor(
                    @JsonProperty("lev") val lev: String?, // 3
                    @JsonProperty("name") val name: String?, // ATV Box Accessories
                    @JsonProperty("popularity") val popularity: String?, // 78.607
                    @JsonProperty("url") val url: String? // atv-boxes-accessories
            )

            data class SpecialOffers @JsonCreator constructor(
                    @JsonProperty("category") val category: Category?,
                    @JsonProperty("is_best_rated") val isBestRated: Int?, // 0
                    @JsonProperty("is_clearance") val isClearance: Int?, // 0
                    @JsonProperty("is_coupon") val isCoupon: Int?, // 0
                    @JsonProperty("is_extra_cashback") val isExtraCashback: Int?, // 0
                    @JsonProperty("is_free_gift") val isFreeGift: Int?, // 0
                    @JsonProperty("is_instant_rebate") val isInstantRebate: Int?, // 0
                    @JsonProperty("is_killer_deal") val isKillerDeal: Int?, // 0
                    @JsonProperty("is_mail_in_rebate") val isMailInRebate: Int?, // 0
                    @JsonProperty("is_new") val isNew: Int?, // 0
                    @JsonProperty("is_outlet") val isOutlet: Int?, // 0
                    @JsonProperty("is_sale") val isSale: Int?, // 0
                    @JsonProperty("is_seasonal") val isSeasonal: Int?, // 0
                    @JsonProperty("is_second_day_air") val isSecondDayAir: Int?, // 0
                    @JsonProperty("is_shed") val isShed: Int?, // 0
                    @JsonProperty("primary") val primary: String?, // plain
//                    @JsonProperty("promotions") val promotions: List<Any?>?,
                    @JsonProperty("special_offer_to_type") val specialOfferToType: SpecialOfferToType?
            ) {
                data class Category @JsonCreator constructor(
                        @JsonProperty("lev") val lev: String?, // 2
                        @JsonProperty("name") val name: String?, // Auto &amp; Atv
                        @JsonProperty("popularity") val popularity: String?, // 87.392
                        @JsonProperty("url") val url: String? // auto-atv
                )

                data class SpecialOfferToType @JsonCreator constructor(
                        @JsonProperty("is_best_rated") val isBestRated: String?, // best_rated
                        @JsonProperty("is_clearance") val isClearance: String?, // clearance
                        @JsonProperty("is_coupon") val isCoupon: String?, // coupon
                        @JsonProperty("is_extra_cashback") val isExtraCashback: String?, // extra_cashback
                        @JsonProperty("is_final_sale") val isFinalSale: String?, // final_sale
                        @JsonProperty("is_free_gift") val isFreeGift: String?, // free_gift
                        @JsonProperty("is_instant_rebate") val isInstantRebate: String?, // instant_rebate
                        @JsonProperty("is_killer_deal") val isKillerDeal: String?, // killer_deal
                        @JsonProperty("is_mail_in_rebate") val isMailInRebate: String?, // mail_in_rebate
                        @JsonProperty("is_new") val isNew: String?, // new
                        @JsonProperty("is_sale") val isSale: String?, // sale
                        @JsonProperty("is_seasonal") val isSeasonal: String?, // marketing
                        @JsonProperty("is_second_day_air") val isSecondDayAir: String? // second_day_air
                )
            }
        }
    }
}