package io.bifri.productcatalog.network.response

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

data class ErrorResponse @JsonCreator constructor(
    @JsonProperty("code") val code: Int?, // 404
    @JsonProperty("message") val message: String?, // Not Found
    @JsonProperty("status") val status: String?, // Not Found
    @JsonProperty("time") val time: Int? // 1619260864
)