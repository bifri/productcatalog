package io.bifri.productcatalog.network.productsservice

import com.fasterxml.jackson.databind.ObjectMapper
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoSet
import io.bifri.productcatalog.BuildConfig
import io.bifri.productcatalog.di.API_KEY_INTERCEPTOR
import io.bifri.productcatalog.di.LOGGING_INTERCEPTOR
import io.bifri.productcatalog.di.PerApp
import io.bifri.productcatalog.exception.ApiExceptionStore
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import javax.inject.Named


@Module class ProductsServiceModule {

    @Provides @PerApp fun provideProductsApi(
            retrofit: Retrofit,
            apiExceptionStore: ApiExceptionStore
    ): ProductsApi = ProductsApi(retrofit)

    @Provides @PerApp fun provideRetrofit(
            client: OkHttpClient,
            callAdapterFactory: CallAdapter.Factory,
            converterFactories: Set<@JvmSuppressWildcards Converter.Factory>
    ): Retrofit = RetrofitGetter(
            client,
            callAdapterFactory,
            converterFactories
    )
            .retrofit

    @Provides @PerApp fun provideRetrofitCallAdapterFactory(): CallAdapter.Factory =
            RxErrorHandlingCallAdapterFactory.create()

    @Provides @IntoSet @PerApp fun provideRetrofitScalarsConverterFactory(): Converter.Factory =
            ScalarsConverterFactory.create()

    @Provides @IntoSet @PerApp fun provideRetrofitJacksonConverterFactory(
            objectMapper: ObjectMapper
    ): Converter.Factory = JacksonConverterFactory.create(objectMapper)

    @Provides @PerApp fun provideOkHttpClient(
            interceptors: Collection<Interceptor>
    ): OkHttpClient = OkHttpClientGetter(interceptors).okHttpClient

    @Provides @PerApp fun provideInterceptors(
            @Named(API_KEY_INTERCEPTOR) apiKeyInterceptor: Interceptor,
            @Named(LOGGING_INTERCEPTOR) loggingInterceptor: Interceptor
    ): Collection<@JvmWildcard Interceptor> = listOf(apiKeyInterceptor, loggingInterceptor)

    @Provides @PerApp @Named(API_KEY_INTERCEPTOR)
    fun provideApiKeyInterceptor(): Interceptor = ApiKeyInterceptor()

    @Provides @PerApp @Named(LOGGING_INTERCEPTOR)
    fun provideLoggingInterceptor(): Interceptor = HttpLoggingInterceptor().apply {
        level = if (BuildConfig.DEBUG) Level.BODY else Level.NONE
    }

}