package io.bifri.productcatalog.network.productsservice

import okhttp3.Interceptor
import okhttp3.Response

private const val API_KEY_HEADER_NAME = "x-apikey"
private const val API_KEY_HEADER_VALUE = "58a12ef1e09a105d95e19f6a5399de6174198b4cbb228365d0c2f8601487fe070346a4905c6cb06f4347b7c6b5a224d592afb88b96c700b400bf5d72be6bfc77"

class ApiKeyInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val newRequest = originalRequest.newBuilder()
                .header(API_KEY_HEADER_NAME, API_KEY_HEADER_VALUE)
                .build()
        return chain.proceed(newRequest)
    }

}