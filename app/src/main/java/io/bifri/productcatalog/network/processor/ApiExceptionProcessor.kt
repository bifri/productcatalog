package io.bifri.productcatalog.network.processor

import io.bifri.productcatalog.exception.ApiException
import io.bifri.productcatalog.exception.ApiExceptionStore
import io.bifri.productcatalog.exception.ExceptionService

class ApiExceptionProcessor(
        private val exceptionService: ExceptionService,
        private val apiExceptionStore: ApiExceptionStore
) {

    fun processApiException(throwable: Throwable) {
        apiExceptionStore.publish(ApiException(throwable))
        exceptionService.processException(throwable)
    }

}