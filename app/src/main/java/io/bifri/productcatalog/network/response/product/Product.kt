package io.bifri.productcatalog.network.response.product
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal


data class Product @JsonCreator constructor(
    @JsonProperty("affiliate_title") val affiliateTitle: String?,
    @JsonProperty("affiliate_url") val affiliateUrl: String?,
    @JsonProperty("all_variants_are_outlet") val allVariantsAreOutlet: Boolean?, // false
    @JsonProperty("all_variants_are_shed") val allVariantsAreShed: Boolean?, // false
    @JsonProperty("anchor_text") val anchorText: String?, // Mad Dog ATV Comfort Ride Seat Protector
    @JsonProperty("answer_count") val answerCount: Int?, // 0
    @JsonProperty("are_min_prices_after_rebate") val areMinPricesAfterRebate: AreMinPricesAfterRebate?,
    @JsonProperty("are_savings_exact") val areSavingsExact: AreSavingsExact?,
    @JsonProperty("are_segment_sale_prices_hidden") val areSegmentSalePricesHidden: AreSegmentSalePricesHidden?,
    @JsonProperty("brand") val brand: Brand?,
    @JsonProperty("brand_categories") val brandCategories: List<BrandCategory?>?,
    @JsonProperty("brand_id") val brandId: Long?, // 6070
    @JsonProperty("brand_name") val brandName: String?, // MadDog
    @JsonProperty("breadcrumbs") val breadcrumbs: List<List<Breadcrumb?>?>?,
    @JsonProperty("bundle_data") val bundleData: BundleData?,
    @JsonProperty("cashback_percentage") val cashbackPercentage: BigDecimal?, // 0
    @JsonProperty("categories") val categories: List<Category?>?,
    @JsonProperty("category_name") val categoryName: String?, // ATV Box Accessories / ATV Boxes / Auto & Atv
    @JsonProperty("cents") val cents: String?, // 99
    @JsonProperty("cents_to") val centsTo: String?, // 99
    @JsonProperty("clean_description") val cleanDescription: String?, // Add some extra comfort and protection to your ride when you snap a Comfort Ride Seat Protector to your ATV. The versatile gear does two jobs on any terrain. The first you'll notice is the soft-molded foam. It adds extra cushion for those rough rides. The seat protector also acts as a cover, stopping existing rips and tears from growing on the seat underneath. It's easy to attach. Just snap the three buckles in place for a secure fit. Soft molded foam adds extra cushion. Covers existing rips and tears and stops them from growing bigger. Three quick release buckles easily secure protector to the seat. Versatile doing two jobs on any terrain. Easy to attach by just snapping the three buckles in place.Package Contents:Mad Dog ATV Comfort Ride Seat ProtectorMad Dog 2000012623: ATV Comfort Ride Seat Protector, 3 Quick Release Buckles, Black
    @JsonProperty("clearanceVariantCount") val clearanceVariantCount: Int?, // 0
    @JsonProperty("commodity_code") val commodityCode: String?,
    @JsonProperty("departments") val departments: List<Int?>?,
    @JsonProperty("description") val description: String?, // <div id="product-caption-block-description" class="product-caption-block"><p>Add some extra comfort and protection to your ride when you snap a Comfort Ride Seat Protector to your ATV. The versatile gear does two jobs on any terrain. The first you'll notice is the soft-molded foam. It adds extra cushion for those rough rides. The seat protector also acts as a cover, stopping existing rips and tears from growing on the seat underneath. It's easy to attach. Just snap the three buckles in place for a secure fit. Soft molded foam adds extra cushion. Covers existing rips and tears and stops them from growing bigger. Three quick release buckles easily secure protector to the seat. Versatile doing two jobs on any terrain. Easy to attach by just snapping the three buckles in place.</p></div><div id="product-caption-block-package-content" class="product-caption-block"><h3>Package Contents:</h3><ul><li><strong>Mad Dog ATV Comfort Ride Seat Protector</strong></li></ul></div><div id="product-caption-block-variant-descriptions" class="product-caption-block"><div id="product-caption-block-variant-description-1524400" class="product-caption-block-variant-description">Mad Dog 2000012623: ATV Comfort Ride Seat Protector, 3 Quick Release Buckles, Black</div></div>
    @JsonProperty("display_coupon_in_grid") val displayCouponInGrid: Boolean?, // false
    @JsonProperty("display_instant_rebate_grid") val displayInstantRebateGrid: Boolean?, // false
    @JsonProperty("display_killer_deal_in_grid") val displayKillerDealInGrid: Boolean?, // false
    @JsonProperty("display_mail_in_rebate_grid") val displayMailInRebateGrid: Boolean?, // false
    @JsonProperty("document_id") val documentId: String?, // 773046
    @JsonProperty("family_id") val familyId: String?,
    @JsonProperty("free_shipping") val freeShipping: Int?, // 0
    @JsonProperty("full_name") val fullName: String?, // Mad Dog ATV Comfort Ride Seat Protector
    @JsonProperty("gift_certificate") val giftCertificate: Int?, // 0
    @JsonProperty("grid_save_percent") val gridSavePercent: BigDecimal?, // 8
    @JsonProperty("grid_save_percents") val gridSavePercents: GridSavePercents?,
    @JsonProperty("h1_name") val h1Name: String?, // Mad Dog ATV Comfort Ride Seat Protector 2000012623  $2.00 Off
    @JsonProperty("h1_name_mobile") val h1NameMobile: String?, // Mad Dog ATV Comfort Ride Seat Protector 2000012623  $2.00 Off
    @JsonProperty("h1_product_info") val h1ProductInfo: String?, // Mad Dog ATV Comfort Ride Seat Protector Product Info
    @JsonProperty("h2_comments") val h2Comments: String?, // Mad Dog ATV Comfort Ride Seat Protector Comments
    @JsonProperty("h2_items") val h2Items: String?, // Related Products to Mad Dog ATV Comfort Ride Seat Protector
    @JsonProperty("h2_similar_items") val h2SimilarItems: String?, // Mad Dog ATV Comfort Ride Seat Protector Similar Items
    @JsonProperty("has_coupons") val hasCoupons: Int?, // 0
    @JsonProperty("has_deals") val hasDeals: Int?, // 0
    @JsonProperty("has_free_gifts") val hasFreeGifts: Int?, // 0
    @JsonProperty("has_manual_description") val hasManualDescription: Int?, // 0
    @JsonProperty("has_options") val hasOptions: Int?, // 0
    @JsonProperty("images") val images: List<Image?>?,
    @JsonProperty("inverted_popularity") val invertedPopularity: Int?, // 53832
    @JsonProperty("is_available_on_op") val isAvailableOnOp: Int?, // 0
    @JsonProperty("is_best") val isBest: Int?, // 1
    @JsonProperty("is_best_rated") val isBestRated: Int?, // 0
    @JsonProperty("is_best_sale") val isBestSale: Int?, // 0
    @JsonProperty("is_call_to_order") val isCallToOrder: Int?, // 0
    @JsonProperty("is_clearance") val isClearance: Int?, // 0
    @JsonProperty("is_demo") val isDemo: Int?, // 0
    @JsonProperty("is_extra_cashback") val isExtraCashback: Boolean?, // false
    @JsonProperty("is_killer_deal") val isKillerDeal: Int?, // 0
    @JsonProperty("is_kit") val isKit: Int?, // 0
    @JsonProperty("is_made_in_usa") val isMadeInUsa: Int?, // 0
    @JsonProperty("is_min_price_after_rebate") val isMinPriceAfterRebate: Boolean?, // false
    @JsonProperty("is_new") val isNew: Int?, // 0
    @JsonProperty("is_orderable") val isOrderable: Int?, // 1
    @JsonProperty("is_outlet") val isOutlet: Int?, // 0
    @JsonProperty("is_rebate") val isRebate: Int?, // 0
    @JsonProperty("is_sale") val isSale: Int?, // 0
    @JsonProperty("is_save_exact") val isSaveExact: Boolean?, // true
    @JsonProperty("is_saving_exact") val isSavingExact: Boolean?, // true
    @JsonProperty("is_second_day_air") val isSecondDayAir: Int?, // 0
    @JsonProperty("is_segment_sale_price_hidden") val isSegmentSalePriceHidden: Int?, // 0
    @JsonProperty("is_shed") val isShed: Int?, // 0
    @JsonProperty("is_show_scrolling_banner") val isShowScrollingBanner: Int?, // 0
    @JsonProperty("is_use_price_per_uom_calculation") val isUsePricePerUomCalculation: Boolean?, // false
    @JsonProperty("items") val items: List<Item?>?,
    @JsonProperty("js_inline") val jsInline: String?,
    @JsonProperty("js_references") val jsReferences: String?,
    @JsonProperty("killerDealVariantCount") val killerDealVariantCount: Int?, // 0
    @JsonProperty("list_price") val listPrice: BigDecimal?, // 23.99
    @JsonProperty("max_final_sale_price") val maxFinalSalePrice: BigDecimal?, // 21.99
    @JsonProperty("max_final_sale_prices") val maxFinalSalePrices: MaxFinalSalePrices?,
    @JsonProperty("max_final_sale_prices_per_uom_number") val maxFinalSalePricesPerUomNumber: MaxFinalSalePricesPerUomNumber?,
    @JsonProperty("max_grid_saving") val maxGridSaving: BigDecimal?, // 2
    @JsonProperty("max_grid_savings") val maxGridSavings: MaxGridSavings?,
    @JsonProperty("max_sale_price") val maxSalePrice: BigDecimal?, // 21.99
    @JsonProperty("max_sale_prices") val maxSalePrices: MaxSalePrices?,
    @JsonProperty("max_saving") val maxSaving: BigDecimal?, // 2
    @JsonProperty("max_savings") val maxSavings: MaxSavings?,
    @JsonProperty("meta_description") val metaDescription: String?, // Shop Mad Dog ATV Comfort Ride Seat Protector | $2.00 Off Be The First To Review Mad Dog ATV Comfort Ride Seat Protector  + Free Shipping over $49.
    @JsonProperty("meta_description_mobile") val metaDescriptionMobile: String?, // Shop Mad Dog ATV Comfort Ride Seat Protector | $2.00 Off Be The First To Review Mad Dog ATV Comfort Ride Seat Protector  + Free Shipping over $49.
    @JsonProperty("meta_keywords") val metaKeywords: String?, // Mad Dog ATV Comfort Ride Seat Protector, MadDog, ATV Box Accessories
    @JsonProperty("min_final_grid_sale_price") val minFinalGridSalePrice: BigDecimal?, // 21.99
    @JsonProperty("min_final_grid_sale_prices") val minFinalGridSalePrices: MinFinalGridSalePrices?,
    @JsonProperty("min_final_sale_price") val minFinalSalePrice: BigDecimal?, // 21.99
    @JsonProperty("min_final_sale_price_uom_labels") val minFinalSalePriceUomLabels: MinFinalSalePriceUomLabels?,
    @JsonProperty("min_final_sale_prices") val minFinalSalePrices: MinFinalSalePrices?,
    @JsonProperty("min_final_sale_prices_per_uom_number") val minFinalSalePricesPerUomNumber: MinFinalSalePricesPerUomNumber?,
    @JsonProperty("min_grid_saving") val minGridSaving: BigDecimal?, // 2
    @JsonProperty("min_grid_savings") val minGridSavings: MinGridSavings?,
    @JsonProperty("min_sale_price") val minSalePrice: BigDecimal?, // 21.99
    @JsonProperty("min_sale_prices") val minSalePrices: MinSalePrices?,
    @JsonProperty("min_saving") val minSaving: BigDecimal?, // 2
    @JsonProperty("min_savings") val minSavings: MinSavings?,
    @JsonProperty("original_product_flags") val originalProductFlags: List<String?>?,
    @JsonProperty("page_title") val pageTitle: String?, // Mad Dog ATV Comfort Ride Seat Protector | $2.00 Off   Free Shipping over $49!
    @JsonProperty("page_title_mobile") val pageTitleMobile: String?, // Mad Dog ATV Comfort Ride Seat Protector | $2.00 Off   Free Shipping over $49!
    @JsonProperty("popularity") val popularity: BigDecimal?, // 46.168
    @JsonProperty("price") val price: BigDecimal?, // 21.99
    @JsonProperty("primary_image") val primaryImage: String?, // opplanet-mad-dog-atv-comfort-ride-seat-protector-2000012623-main
    @JsonProperty("product_code") val productCode: String?, // 2GZ-AA-2000012623
    @JsonProperty("product_id") val productId: Long?, // 773046
    @JsonProperty("prop65_messages") val prop65Messages: Prop65Messages?,
    @JsonProperty("question_count") val questionCount: Int?, // 2
    @JsonProperty("rad_eligible") val radEligible: Boolean?, // false
    @JsonProperty("rad_eligibles") val radEligibles: RadEligibles?,
    @JsonProperty("raw_product") val rawProduct: Int?, // 1
    @JsonProperty("rebate_amount") val rebateAmount: BigDecimal?, // 0
    @JsonProperty("related_info_pages") val relatedInfoPages: List<String?>?,
    @JsonProperty("review_count") val reviewCount: Int?, // 0
    @JsonProperty("review_rating") val reviewRating: String?, // 0
    @JsonProperty("review_rating_weighted") val reviewRatingWeighted: String?, // 0
    @JsonProperty("reviews") val reviews: Reviews?,
    @JsonProperty("rounded_rating") val roundedRating: Int?, // 0
    @JsonProperty("save_amount") val saveAmount: BigDecimal?, // 2
    @JsonProperty("save_percent") val savePercent: BigDecimal?, // 8
    @JsonProperty("save_percents") val savePercents: SavePercents?,
    @JsonProperty("savings_text") val savingsText: String?, // Save $2.00
    @JsonProperty("short_name") val shortName: String?, // ATV Comfort Ride Seat Protector
    @JsonProperty("show_as_low_as") val showAsLowAs: Boolean?, // false
    @JsonProperty("show_return_policy") val showReturnPolicy: Boolean?, // true
    @JsonProperty("side_panel_items") val sidePanelItems: List<SidePanelItem?>?,
    @JsonProperty("special_offers") val specialOffers: SpecialOffers?,
    @JsonProperty("total_variant_count") val totalVariantCount: Int?, // 1
    @JsonProperty("unorderable_type_id") val unorderableTypeId: String?,
    @JsonProperty("unorderable_variant_count") val unorderableVariantCount: Int?, // 0
    @JsonProperty("url") val url: String?, // mad-dog-atv-comfort-ride-seat-protector
    @JsonProperty("variant_count") val variantCount: Int?, // 1
    @JsonProperty("variants") val variants: List<Variant?>?,
    @JsonProperty("variantsMaxListPrice") val variantsMaxListPrice: BigDecimal?, // 23.99
    @JsonProperty("version") val version: Int?, // 688
    @JsonProperty("weight") val weight: BigDecimal?, // 1
    @JsonProperty("whole") val whole: String?, // 21
    @JsonProperty("whole_to") val wholeTo: String? // 21
) {
    data class AreMinPricesAfterRebate @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: Boolean?, // false
        @JsonProperty("offhours") val offhours: Boolean?, // false
        @JsonProperty("regular") val regular: Boolean?, // false
        @JsonProperty("test_evasive") val testEvasive: Boolean?, // false
        @JsonProperty("test_offhours") val testOffhours: Boolean?, // false
        @JsonProperty("test_regular") val testRegular: Boolean? // false
    )

    data class AreSavingsExact @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: Boolean?, // true
        @JsonProperty("offhours") val offhours: Boolean?, // true
        @JsonProperty("regular") val regular: Boolean?, // true
        @JsonProperty("test_evasive") val testEvasive: Boolean?, // true
        @JsonProperty("test_offhours") val testOffhours: Boolean?, // true
        @JsonProperty("test_regular") val testRegular: Boolean? // true
    )

    data class AreSegmentSalePricesHidden @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: BigDecimal?, // 0
        @JsonProperty("offhours") val offhours: BigDecimal?, // 0
        @JsonProperty("regular") val regular: BigDecimal?, // 0
        @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 0
        @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 0
        @JsonProperty("test_regular") val testRegular: BigDecimal? // 0
    )

    data class Brand @JsonCreator constructor(
        @JsonProperty("full_name") val fullName: String?, // MadDog
        @JsonProperty("images") val images: List<Image?>?,
        @JsonProperty("url") val url: String? // mad-dog-brand
    ) {
        data class Image @JsonCreator constructor(
            @JsonProperty("source") val source: String?, // brand
            @JsonProperty("title") val title: String?, // MadDog 2019 Logo
            @JsonProperty("url") val url: String? // opplanet-maddog-2019-logo
        )
    }

    data class BrandCategory @JsonCreator constructor(
        @JsonProperty("brand_id") val brandId: String?, // 6070
        @JsonProperty("brand_name") val brandName: String?, // MadDog
        @JsonProperty("category_id") val categoryId: String?, // 3
        @JsonProperty("category_name") val categoryName: String?, // ATV Box Accessories
        @JsonProperty("facet") val facet: String?, // Mad Dog ATV Box Accessories:===:177256:===:mad-dog-atv-boxes-accessories:===:full_name=Mad Dog ATV Box Accessories:===:brand_name=MadDog:===:brand_full_name=MadDog:===:category_name=ATV Box Accessories
        @JsonProperty("id") val id: String?, // 177256
        @JsonProperty("inverted_popularity") val invertedPopularity: Int?, // 58500
        @JsonProperty("name") val name: String?, // Mad Dog ATV Box Accessories
        @JsonProperty("orderable_count") val orderableCount: String?, // 7
        @JsonProperty("parent_category_id") val parentCategoryId: String?, // 72
        @JsonProperty("popularity") val popularity: BigDecimal?, // 41.5
        @JsonProperty("url") val url: String? // mad-dog-atv-boxes-accessories
    )

    data class Breadcrumb @JsonCreator constructor(
        @JsonProperty("separator") val separator: String?, // arrow
        @JsonProperty("title") val title: String?, // MadDog
        @JsonProperty("url") val url: String? // mad-dog-brand
    )

    data class BundleData @JsonCreator constructor(
        @JsonProperty("bundle") val bundle: List<Bundle?>?,
        @JsonProperty("bundleMaxPrice") val bundleMaxPrice: BigDecimal?, // 0
        @JsonProperty("bundleMessage") val bundleMessage: String?,
        @JsonProperty("bundleStartPrice") val bundleStartPrice: BigDecimal?, // 0
        @JsonProperty("hasTooLowToShow") val hasTooLowToShow: Boolean?, // false
        @JsonProperty("maxVariantSaveOriginal") val maxVariantSaveOriginal: BigDecimal?, // 0
        @JsonProperty("maxVariantSaveSale") val maxVariantSaveSale: BigDecimal?, // 0
        @JsonProperty("savePercent") val savePercent: BigDecimal? // 0
    ) {
        data class Bundle @JsonCreator constructor(
            @JsonProperty("brand_name") val brandName: String?,
            @JsonProperty("bundle_id") val bundleId: String?,
            @JsonProperty("category_name") val categoryName: String?,
            @JsonProperty("full_name") val fullName: String?,
            @JsonProperty("group_label") val groupLabel: String?,
            @JsonProperty("has_coupons") val hasCoupons: Int?, // 0
            @JsonProperty("has_promos") val hasPromos: Boolean?, // false
            @JsonProperty("image") val image: String?, // opplanet-mad-dog-atv-comfort-ride-seat-protector-2000012623-main
            @JsonProperty("is_disabled") val isDisabled: Boolean?, // true
            @JsonProperty("is_free") val isFree: Boolean?, // true
            @JsonProperty("is_second_day_air") val isSecondDayAir: Int?, // 0
            @JsonProperty("original_price") val originalPrice: BigDecimal?, // 0
            @JsonProperty("price") val price: BigDecimal?, // 0
            @JsonProperty("price_prefix") val pricePrefix: String?,
            @JsonProperty("product_code") val productCode: String?,
            @JsonProperty("sku") val sku: String?, // 2GZ-AA-2000012623
            @JsonProperty("url") val url: String?, // mad-dog-atv-comfort-ride-seat-protector
            @JsonProperty("variant_id") val variantId: String?, // 1524400
            @JsonProperty("variant_name") val variantName: String? // Mad Dog ATV Comfort Ride Seat Protector, 3 Quick Release Buckles, Black 2000012623
        )
    }

    data class Category @JsonCreator constructor(
        @JsonProperty("category_id") val categoryId: String?, // 3
        @JsonProperty("full_name") val fullName: String?, // ATV Box Accessories
        @JsonProperty("left_index") val leftIndex: String?, // 49
        @JsonProperty("right_index") val rightIndex: String?, // 50
        @JsonProperty("url") val url: String? // atv-boxes-accessories
    )

    data class GridSavePercents @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: BigDecimal?, // 8
        @JsonProperty("offhours") val offhours: BigDecimal?, // 8
        @JsonProperty("regular") val regular: BigDecimal?, // 8
        @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 8
        @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 8
        @JsonProperty("test_regular") val testRegular: BigDecimal? // 8
    )

    data class Image @JsonCreator constructor(
        @JsonProperty("source") val source: String?, // variant
        @JsonProperty("title") val title: String?, // Coleman Mad Dog ATV Comfort Ride Seat Protector, 3 Quick Release Buckles, Black 2000012623
        @JsonProperty("url") val url: String?, // opplanet-mad-dog-atv-comfort-ride-seat-protector-2000012623-main
        @JsonProperty("variant_id") val variantId: Int? // 1524400
    )

    data class Item @JsonCreator constructor(
        @JsonProperty("anchor_text") val anchorText: String?, // Steiner M5Xi 5-25x56mm Military Riflescope
        @JsonProperty("are_min_prices_after_rebate") val areMinPricesAfterRebate: AreMinPricesAfterRebate?,
        @JsonProperty("are_savings_exact") val areSavingsExact: AreSavingsExact?,
        @JsonProperty("are_segment_sale_prices_hidden") val areSegmentSalePricesHidden: AreSegmentSalePricesHidden?,
        @JsonProperty("brand") val brand: Brand?,
        @JsonProperty("brand_id") val brandId: Int?, // 46
        @JsonProperty("brand_name") val brandName: String?, // Steiner
        @JsonProperty("cashback_percentage") val cashbackPercentage: BigDecimal?, // 0
        @JsonProperty("categories") val categories: List<Category?>?,
        @JsonProperty("category_name") val categoryName: String?, // Riflescopes / Riflescopes & Accessories
        @JsonProperty("cents") val cents: String?, // 00
        @JsonProperty("cents_to") val centsTo: String?, // 99
        @JsonProperty("display_coupon_in_grid") val displayCouponInGrid: Boolean?, // false
        @JsonProperty("display_free_gifts_grid") val displayFreeGiftsGrid: Boolean?, // true
        @JsonProperty("display_instant_rebate_grid") val displayInstantRebateGrid: Boolean?, // false
        @JsonProperty("display_killer_deal_in_grid") val displayKillerDealInGrid: Boolean?, // true
        @JsonProperty("display_mail_in_rebate_grid") val displayMailInRebateGrid: Boolean?, // false
        @JsonProperty("document_id") val documentId: String?, // 565629
        @JsonProperty("flags") val flags: List<Flag?>?,
        @JsonProperty("full_name") val fullName: String?, // Steiner M5Xi 5-25x56mm Military Riflescope
        @JsonProperty("grid_save_percent") val gridSavePercent: BigDecimal?, // 34
        @JsonProperty("grid_save_percents") val gridSavePercents: GridSavePercents?,
        @JsonProperty("has_free_gifts") val hasFreeGifts: Int?, // 1
        @JsonProperty("has_options") val hasOptions: Int?, // 1
        @JsonProperty("is_best") val isBest: Int?, // 1
        @JsonProperty("is_best_rated") val isBestRated: Int?, // 1
        @JsonProperty("is_call_to_order") val isCallToOrder: Int?, // 0
        @JsonProperty("is_clearance") val isClearance: Int?, // 0
        @JsonProperty("is_demo") val isDemo: Int?, // 0
        @JsonProperty("is_extra_cashback") val isExtraCashback: Boolean?, // false
        @JsonProperty("is_killer_deal") val isKillerDeal: Int?, // 1
        @JsonProperty("is_made_in_usa") val isMadeInUsa: Int?, // 0
        @JsonProperty("is_min_price_after_rebate") val isMinPriceAfterRebate: Boolean?, // false
        @JsonProperty("is_new") val isNew: Int?, // 0
        @JsonProperty("is_orderable") val isOrderable: Int?, // 1
        @JsonProperty("is_outlet") val isOutlet: Int?, // 0
        @JsonProperty("is_rebate") val isRebate: Int?, // 0
        @JsonProperty("is_sale") val isSale: Int?, // 0
        @JsonProperty("is_save_exact") val isSaveExact: Boolean?, // false
        @JsonProperty("is_saving_exact") val isSavingExact: Boolean?, // false
        @JsonProperty("is_second_day_air") val isSecondDayAir: Int?, // 1
        @JsonProperty("is_segment_sale_price_hidden") val isSegmentSalePriceHidden: Int?, // 0
        @JsonProperty("is_shed") val isShed: Int?, // 0
        @JsonProperty("is_use_price_per_uom_calculation") val isUsePricePerUomCalculation: Boolean?, // false
        @JsonProperty("item_flag") val itemFlag: String?, // killer-deal
        @JsonProperty("list_price") val listPrice: Any?, // null
        @JsonProperty("max_final_sale_price") val maxFinalSalePrice: BigDecimal?, // 3599.99
        @JsonProperty("max_final_sale_prices") val maxFinalSalePrices: MaxFinalSalePrices?,
        @JsonProperty("max_final_sale_prices_per_uom_number") val maxFinalSalePricesPerUomNumber: MaxFinalSalePricesPerUomNumber?,
        @JsonProperty("max_grid_saving") val maxGridSaving: BigDecimal?, // 1377.99
        @JsonProperty("max_grid_savings") val maxGridSavings: MaxGridSavings?,
        @JsonProperty("max_sale_price") val maxSalePrice: BigDecimal?, // 3599.99
        @JsonProperty("max_sale_prices") val maxSalePrices: MaxSalePrices?,
        @JsonProperty("max_saving") val maxSaving: BigDecimal?, // 1377.99
        @JsonProperty("max_savings") val maxSavings: MaxSavings?,
        @JsonProperty("min_final_grid_sale_price") val minFinalGridSalePrice: BigDecimal?, // 2699
        @JsonProperty("min_final_grid_sale_prices") val minFinalGridSalePrices: MinFinalGridSalePrices?,
        @JsonProperty("min_final_sale_price") val minFinalSalePrice: BigDecimal?, // 2699
        @JsonProperty("min_final_sale_price_uom_labels") val minFinalSalePriceUomLabels: MinFinalSalePriceUomLabels?,
        @JsonProperty("min_final_sale_prices") val minFinalSalePrices: MinFinalSalePrices?,
        @JsonProperty("min_final_sale_prices_per_uom_number") val minFinalSalePricesPerUomNumber: MinFinalSalePricesPerUomNumber?,
        @JsonProperty("min_grid_saving") val minGridSaving: BigDecimal?, // 480.99
        @JsonProperty("min_grid_savings") val minGridSavings: MinGridSavings?,
        @JsonProperty("min_sale_price") val minSalePrice: BigDecimal?, // 2699
        @JsonProperty("min_sale_prices") val minSalePrices: MinSalePrices?,
        @JsonProperty("min_saving") val minSaving: BigDecimal?, // 480.99
        @JsonProperty("min_savings") val minSavings: MinSavings?,
        @JsonProperty("popularity") val popularity: BigDecimal?, // 98.778
        @JsonProperty("price") val price: BigDecimal?, // 2699
        @JsonProperty("primary_image") val primaryImage: String?, // opplanet-steiner-5-25x56-m5xi-military-riflescope-mcimage-spids-94567-94732-vids
        @JsonProperty("product_code") val productCode: String?, // ST-RS-TASTR
        @JsonProperty("product_id") val productId: Long?, // 565629
        @JsonProperty("promotions") val promotions: Promotions?,
        @JsonProperty("rad_eligible") val radEligible: Boolean?, // false
        @JsonProperty("rebate_amount") val rebateAmount: BigDecimal?, // 0
        @JsonProperty("review_count") val reviewCount: Int?, // 5
        @JsonProperty("review_rating") val reviewRating: String?, // 5.0000
        @JsonProperty("ribbon_flags") val ribbonFlags: List<RibbonFlag?>?,
        @JsonProperty("save_amount") val saveAmount: BigDecimal?, // 1377.99
        @JsonProperty("save_percent") val savePercent: BigDecimal?, // 34
        @JsonProperty("save_percents") val savePercents: SavePercents?,
        @JsonProperty("savings_text") val savingsText: String?, // Save Up to 34%
        @JsonProperty("short_name") val shortName: String?, // 5-25x56 M5Xi Military Riflescope
        @JsonProperty("show_as_low_as") val showAsLowAs: Boolean?, // true
        @JsonProperty("unorderable_variants") val unorderableVariants: List<UnorderableVariant?>?,
        @JsonProperty("url") val url: String?, // steiner-5-25x56mm-rifle-scope-w-illuminated-reticles
        @JsonProperty("variant_count") val variantCount: Int?, // 6
        @JsonProperty("variants") val variants: List<Variant?>?,
        @JsonProperty("whole") val whole: String?, // 2699
        @JsonProperty("whole_to") val wholeTo: String? // 3599
    ) {
        data class AreMinPricesAfterRebate @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: Boolean?, // false
            @JsonProperty("offhours") val offhours: Boolean?, // false
            @JsonProperty("regular") val regular: Boolean?, // false
            @JsonProperty("test_evasive") val testEvasive: Boolean?, // false
            @JsonProperty("test_offhours") val testOffhours: Boolean?, // false
            @JsonProperty("test_regular") val testRegular: Boolean? // false
        )

        data class AreSavingsExact @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: Boolean?, // false
            @JsonProperty("offhours") val offhours: Boolean?, // false
            @JsonProperty("regular") val regular: Boolean?, // false
            @JsonProperty("test_evasive") val testEvasive: Boolean?, // false
            @JsonProperty("test_offhours") val testOffhours: Boolean?, // false
            @JsonProperty("test_regular") val testRegular: Boolean? // false
        )

        data class AreSegmentSalePricesHidden @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 0
            @JsonProperty("offhours") val offhours: BigDecimal?, // 0
            @JsonProperty("regular") val regular: BigDecimal?, // 0
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 0
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 0
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 0
        )

        data class Brand @JsonCreator constructor(
            @JsonProperty("facet") val facet: String?, // Steiner:===:46:===:steiner-brand:===:slug=steiner:===:featured=1
            @JsonProperty("id") val id: Long?, // 46
            @JsonProperty("images") val images: List<Image?>?,
            @JsonProperty("inverted_popularity") val invertedPopularity: BigDecimal?, // 2406.000000000006
            @JsonProperty("name") val name: String?, // Steiner
            @JsonProperty("popularity") val popularity: String?, // 97.594
            @JsonProperty("slug") val slug: String?, // steiner
            @JsonProperty("url") val url: String? // steiner-brand
        ) {
            data class Image @JsonCreator constructor(
                @JsonProperty("source") val source: String?, // brand
                @JsonProperty("title") val title: String?, // Steiner Brand Logo Feb 2014
                @JsonProperty("url") val url: String? // opplanet-steiner-brand-logo-2-2014
            )
        }

        data class Category @JsonCreator constructor(
            @JsonProperty("facet") val facet: String?, // Riflescopes:===:21:===:riflescopes:===:slug=riflescopes:===:featured=1
            @JsonProperty("id") val id: String?, // 21
            @JsonProperty("inverted_popularity") val invertedPopularity: BigDecimal?, // 864.9999999999949
            @JsonProperty("lev") val lev: String?, // 3
            @JsonProperty("name") val name: String?, // Riflescopes
            @JsonProperty("parent_id") val parentId: String?, // 4431
            @JsonProperty("popularity") val popularity: String?, // 99.135
            @JsonProperty("product_count") val productCount: Int?, // 1847
            @JsonProperty("slug") val slug: String?, // riflescopes
            @JsonProperty("url") val url: String? // riflescopes
        )

        data class Flag @JsonCreator constructor(
            @JsonProperty("facet") val facet: String?, // Best Rated:===::===:best-rated
            @JsonProperty("name") val name: String?, // Best Rated
            @JsonProperty("url") val url: String? // best-rated
        )

        data class GridSavePercents @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 34
            @JsonProperty("offhours") val offhours: BigDecimal?, // 34
            @JsonProperty("regular") val regular: BigDecimal?, // 34
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 34
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 34
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 34
        )

        data class MaxFinalSalePrices @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 3599.99
            @JsonProperty("offhours") val offhours: BigDecimal?, // 3599.99
            @JsonProperty("regular") val regular: BigDecimal?, // 3599.99
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 3599.99
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 3599.99
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 3599.99
        )

        data class MaxFinalSalePricesPerUomNumber @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 3599.99
            @JsonProperty("offhours") val offhours: BigDecimal?, // 3599.99
            @JsonProperty("regular") val regular: BigDecimal?, // 3599.99
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 3599.99
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 3599.99
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 3599.99
        )

        data class MaxGridSavings @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 1377.99
            @JsonProperty("offhours") val offhours: BigDecimal?, // 1377.99
            @JsonProperty("regular") val regular: BigDecimal?, // 1377.99
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 1377.99
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 1377.99
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 1377.99
        )

        data class MaxSalePrices @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 3599.99
            @JsonProperty("offhours") val offhours: BigDecimal?, // 3599.99
            @JsonProperty("regular") val regular: BigDecimal?, // 3599.99
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 3599.99
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 3599.99
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 3599.99
        )

        data class MaxSavings @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 1377.99
            @JsonProperty("offhours") val offhours: BigDecimal?, // 1377.99
            @JsonProperty("regular") val regular: BigDecimal?, // 1377.99
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 1377.99
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 1377.99
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 1377.99
        )

        data class MinFinalGridSalePrices @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 2699
            @JsonProperty("offhours") val offhours: BigDecimal?, // 2699
            @JsonProperty("regular") val regular: BigDecimal?, // 2699
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 2699
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 2699
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 2699
        )

        data class MinFinalSalePriceUomLabels @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: String?,
            @JsonProperty("offhours") val offhours: String?,
            @JsonProperty("regular") val regular: String?,
            @JsonProperty("test_evasive") val testEvasive: String?,
            @JsonProperty("test_offhours") val testOffhours: String?,
            @JsonProperty("test_regular") val testRegular: String?
        )

        data class MinFinalSalePrices @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 2699
            @JsonProperty("offhours") val offhours: BigDecimal?, // 2699
            @JsonProperty("regular") val regular: BigDecimal?, // 2699
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 2699
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 2699
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 2699
        )

        data class MinFinalSalePricesPerUomNumber @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 2699
            @JsonProperty("offhours") val offhours: BigDecimal?, // 2699
            @JsonProperty("regular") val regular: BigDecimal?, // 2699
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 2699
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 2699
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 2699
        )

        data class MinGridSavings @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 480.99
            @JsonProperty("offhours") val offhours: BigDecimal?, // 480.99
            @JsonProperty("regular") val regular: BigDecimal?, // 480.99
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 480.99
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 480.99
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 480.99
        )

        data class MinSalePrices @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 2699
            @JsonProperty("offhours") val offhours: BigDecimal?, // 2699
            @JsonProperty("regular") val regular: BigDecimal?, // 2699
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 2699
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 2699
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 2699
        )

        data class MinSavings @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 480.99
            @JsonProperty("offhours") val offhours: BigDecimal?, // 480.99
            @JsonProperty("regular") val regular: BigDecimal?, // 480.99
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 480.99
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 480.99
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 480.99
        )

        data class Promotions @JsonCreator constructor(
            @JsonProperty("free_gift") val freeGift: List<FreeGift?>?,
            @JsonProperty("instant_rebate") val instantRebate: List<InstantRebate?>?,
            @JsonProperty("killer_deal") val killerDeal: List<KillerDeal?>?,
            @JsonProperty("marketing") val marketing: List<Marketing?>?
        ) {
            data class FreeGift @JsonCreator constructor(
                @JsonProperty("end") val end: String?, // 2021-05-01 00:00:00
                @JsonProperty("free_gift_ids") val freeGiftIds: List<Int?>?,
                @JsonProperty("is_hidden") val isHidden: Boolean?, // false
                @JsonProperty("on_product_grid") val onProductGrid: Int?, // 1
                @JsonProperty("on_product_page") val onProductPage: Int?, // 1
                @JsonProperty("on_promo_page") val onPromoPage: Int?, // 1
                @JsonProperty("on_promotion_page") val onPromotionPage: Int?, // 1
                @JsonProperty("promotion_id") val promotionId: Int?, // 84767
                @JsonProperty("start") val start: String?, // 2021-01-27 00:00:00
                @JsonProperty("type") val type: String? // FG
            )

            data class InstantRebate @JsonCreator constructor(
                @JsonProperty("display_rebate_amount") val displayRebateAmount: DisplayRebateAmount?,
                @JsonProperty("end") val end: String?, // 2021-05-01 00:00:00
                @JsonProperty("has_different_amounts") val hasDifferentAmounts: Boolean?, // true
                @JsonProperty("is_hidden") val isHidden: Boolean?, // false
                @JsonProperty("max_rebate_amount") val maxRebateAmount: BigDecimal?, // 49.8
                @JsonProperty("min_rebate_amount") val minRebateAmount: BigDecimal?, // 35.85
                @JsonProperty("on_product_grid") val onProductGrid: Int?, // 1
                @JsonProperty("on_product_page") val onProductPage: Int?, // 1
                @JsonProperty("on_promo_page") val onPromoPage: Int?, // 1
                @JsonProperty("on_promotion_page") val onPromotionPage: Int?, // 1
                @JsonProperty("promotion_id") val promotionId: Int?, // 86277
                @JsonProperty("rebate_amount") val rebateAmount: Any?, // null
                @JsonProperty("rebate_amounts") val rebateAmounts: List<BigDecimal?>?,
                @JsonProperty("start") val start: String?, // 2021-04-01 00:00:00
                @JsonProperty("type") val type: String? // RI
            ) {
                data class DisplayRebateAmount @JsonCreator constructor(
                    @JsonProperty("amount") val amount: Int?, // 20
                    @JsonProperty("amount_type") val amountType: String? // percent
                )
            }

            data class KillerDeal @JsonCreator constructor(
                @JsonProperty("end") val end: String?, // 2021-04-28 00:00:00
                @JsonProperty("is_hidden") val isHidden: Boolean?, // false
                @JsonProperty("on_product_grid") val onProductGrid: Int?, // 1
                @JsonProperty("on_product_page") val onProductPage: Int?, // 1
                @JsonProperty("on_promo_page") val onPromoPage: Int?, // 1
                @JsonProperty("on_promotion_page") val onPromotionPage: Int?, // 1
                @JsonProperty("promotion_id") val promotionId: Int?, // 85875
                @JsonProperty("start") val start: String?, // 2021-04-12 00:00:00
                @JsonProperty("type") val type: String? // KD
            )

            data class Marketing @JsonCreator constructor(
                @JsonProperty("end") val end: String?, // 2021-05-03 00:00:00
                @JsonProperty("is_hidden") val isHidden: Boolean?, // false
                @JsonProperty("on_product_grid") val onProductGrid: Int?, // 1
                @JsonProperty("on_product_page") val onProductPage: Int?, // 1
                @JsonProperty("on_promo_page") val onPromoPage: Int?, // 1
                @JsonProperty("on_promotion_page") val onPromotionPage: Int?, // 1
                @JsonProperty("promotion_id") val promotionId: Int?, // 85899
                @JsonProperty("start") val start: String?, // 2021-04-19 00:00:00
                @JsonProperty("type") val type: String? // MK
            )
        }

        data class RibbonFlag @JsonCreator constructor(
            @JsonProperty("item_flag") val itemFlag: String?, // killer-deal
            @JsonProperty("price_flag") val priceFlag: String?, // Killer Deal
            @JsonProperty("priority") val priority: String? // high-priority
        )

        data class SavePercents @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 34
            @JsonProperty("offhours") val offhours: BigDecimal?, // 34
            @JsonProperty("regular") val regular: BigDecimal?, // 34
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 34
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 34
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 34
        )

        data class UnorderableVariant @JsonCreator constructor(
            @JsonProperty("document_id") val documentId: String?, // 1912905
            @JsonProperty("has_filterable_specifications") val hasFilterableSpecifications: Boolean?, // false
            @JsonProperty("is_orderable") val isOrderable: String?, // 0
            @JsonProperty("product_id") val productId: String? // 565629
        )

        data class Variant @JsonCreator constructor(
            @JsonProperty("document_id") val documentId: String?, // 1593375
            @JsonProperty("has_filterable_specifications") val hasFilterableSpecifications: Boolean?, // false
            @JsonProperty("is_orderable") val isOrderable: String?, // 1
            @JsonProperty("product_id") val productId: String?, // 565629
            @JsonProperty("stock_availability") val stockAvailability: StockAvailability?
        ) {
            data class StockAvailability @JsonCreator constructor(
                @JsonProperty("availability") val availability: String?, // 2 left, order now! Ships in 1-4 days.
                @JsonProperty("max_days_to_ship") val maxDaysToShip: Int?, // 3
                @JsonProperty("qoh") val qoh: Int? // 2
            )
        }
    }

    data class MaxFinalSalePrices @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: BigDecimal?, // 21.99
        @JsonProperty("offhours") val offhours: BigDecimal?, // 21.99
        @JsonProperty("regular") val regular: BigDecimal?, // 21.99
        @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 21.99
        @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 21.99
        @JsonProperty("test_regular") val testRegular: BigDecimal? // 21.99
    )

    data class MaxFinalSalePricesPerUomNumber @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: BigDecimal?, // 21.99
        @JsonProperty("offhours") val offhours: BigDecimal?, // 21.99
        @JsonProperty("regular") val regular: BigDecimal?, // 21.99
        @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 21.99
        @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 21.99
        @JsonProperty("test_regular") val testRegular: BigDecimal? // 21.99
    )

    data class MaxGridSavings @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: BigDecimal?, // 2
        @JsonProperty("offhours") val offhours: BigDecimal?, // 2
        @JsonProperty("regular") val regular: BigDecimal?, // 2
        @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 2
        @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 2
        @JsonProperty("test_regular") val testRegular: BigDecimal? // 2
    )

    data class MaxSalePrices @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: BigDecimal?, // 21.99
        @JsonProperty("offhours") val offhours: BigDecimal?, // 21.99
        @JsonProperty("regular") val regular: BigDecimal?, // 21.99
        @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 21.99
        @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 21.99
        @JsonProperty("test_regular") val testRegular: BigDecimal? // 21.99
    )

    data class MaxSavings @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: BigDecimal?, // 2
        @JsonProperty("offhours") val offhours: BigDecimal?, // 2
        @JsonProperty("regular") val regular: BigDecimal?, // 2
        @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 2
        @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 2
        @JsonProperty("test_regular") val testRegular: BigDecimal? // 2
    )

    data class MinFinalGridSalePrices @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: BigDecimal?, // 21.99
        @JsonProperty("offhours") val offhours: BigDecimal?, // 21.99
        @JsonProperty("regular") val regular: BigDecimal?, // 21.99
        @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 21.99
        @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 21.99
        @JsonProperty("test_regular") val testRegular: BigDecimal? // 21.99
    )

    data class MinFinalSalePriceUomLabels @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: String?,
        @JsonProperty("offhours") val offhours: String?,
        @JsonProperty("regular") val regular: String?,
        @JsonProperty("test_evasive") val testEvasive: String?,
        @JsonProperty("test_offhours") val testOffhours: String?,
        @JsonProperty("test_regular") val testRegular: String?
    )

    data class MinFinalSalePrices @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: BigDecimal?, // 21.99
        @JsonProperty("offhours") val offhours: BigDecimal?, // 21.99
        @JsonProperty("regular") val regular: BigDecimal?, // 21.99
        @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 21.99
        @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 21.99
        @JsonProperty("test_regular") val testRegular: BigDecimal? // 21.99
    )

    data class MinFinalSalePricesPerUomNumber @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: BigDecimal?, // 21.99
        @JsonProperty("offhours") val offhours: BigDecimal?, // 21.99
        @JsonProperty("regular") val regular: BigDecimal?, // 21.99
        @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 21.99
        @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 21.99
        @JsonProperty("test_regular") val testRegular: BigDecimal? // 21.99
    )

    data class MinGridSavings @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: BigDecimal?, // 2
        @JsonProperty("offhours") val offhours: BigDecimal?, // 2
        @JsonProperty("regular") val regular: BigDecimal?, // 2
        @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 2
        @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 2
        @JsonProperty("test_regular") val testRegular: BigDecimal? // 2
    )

    data class MinSalePrices @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: BigDecimal?, // 21.99
        @JsonProperty("offhours") val offhours: BigDecimal?, // 21.99
        @JsonProperty("regular") val regular: BigDecimal?, // 21.99
        @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 21.99
        @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 21.99
        @JsonProperty("test_regular") val testRegular: BigDecimal? // 21.99
    )

    data class MinSavings @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: BigDecimal?, // 2
        @JsonProperty("offhours") val offhours: BigDecimal?, // 2
        @JsonProperty("regular") val regular: BigDecimal?, // 2
        @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 2
        @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 2
        @JsonProperty("test_regular") val testRegular: BigDecimal? // 2
    )

    data class Prop65Messages @JsonCreator constructor(
        @JsonProperty("icon") val icon: String?, // https://opl.0ps.us/images/option-sets/warning.png
        @JsonProperty("label") val label: String?, // WARNING: California`s Proposition 65
        @JsonProperty("messages") val messages: List<String?>?,
        @JsonProperty("popup_subtitle") val popupSubtitle: String?, // For more information go to www.p65warnings.ca.gov
        @JsonProperty("popup_title") val popupTitle: String? // Proposition 65 Warning for California Consumers
    )

    data class RadEligibles @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: Boolean?, // false
        @JsonProperty("offhours") val offhours: Boolean?, // false
        @JsonProperty("regular") val regular: Boolean?, // false
        @JsonProperty("test_evasive") val testEvasive: Boolean?, // false
        @JsonProperty("test_offhours") val testOffhours: Boolean?, // false
        @JsonProperty("test_regular") val testRegular: Boolean? // false
    )

    data class Reviews @JsonCreator constructor(
        @JsonProperty("//www.opticsplanet.com/reviews/") val wwwOpticsplanetComreviews: String?, // All Customer Product Reviews
        @JsonProperty("//www.opticsplanet.com/reviews/reviews-atv-boxes.html") val wwwOpticsplanetComreviewsreviewsAtvBoxesHtml: String?, // All ATV Boxes Reviews
        @JsonProperty("//www.opticsplanet.com/reviews/reviews-mad-dog-atv-boxes.html") val wwwOpticsplanetComreviewsreviewsMadDogAtvBoxesHtml: String?, // All Mad Dog ATV Boxes Reviews
        @JsonProperty("//www.opticsplanet.com/reviews/reviews-mad-dog-brand.html") val wwwOpticsplanetComreviewsreviewsMadDogBrandHtml: String? // All MadDog Reviews
    )

    data class SavePercents @JsonCreator constructor(
        @JsonProperty("evasive") val evasive: BigDecimal?, // 8
        @JsonProperty("offhours") val offhours: BigDecimal?, // 8
        @JsonProperty("regular") val regular: BigDecimal?, // 8
        @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 8
        @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 8
        @JsonProperty("test_regular") val testRegular: BigDecimal? // 8
    )

    data class SidePanelItem @JsonCreator constructor(
        @JsonProperty("anchor_text") val anchorText: String?, // Trijicon MRO 1x25mm 2 MOA Reticle Red Dot Sight
        @JsonProperty("are_min_prices_after_rebate") val areMinPricesAfterRebate: AreMinPricesAfterRebate?,
        @JsonProperty("are_savings_exact") val areSavingsExact: AreSavingsExact?,
        @JsonProperty("are_segment_sale_prices_hidden") val areSegmentSalePricesHidden: AreSegmentSalePricesHidden?,
        @JsonProperty("brand") val brand: Brand?,
        @JsonProperty("brand_id") val brandId: Long?, // 70
        @JsonProperty("brand_name") val brandName: String?, // Trijicon
        @JsonProperty("cashback_percentage") val cashbackPercentage: BigDecimal?, // 0
        @JsonProperty("categories") val categories: List<Category?>?,
        @JsonProperty("category_name") val categoryName: String?, // Red Dot Sights / Red Dot Sights & Accessories
        @JsonProperty("cents") val cents: String?, // 49
        @JsonProperty("cents_to") val centsTo: String?, // 99
        @JsonProperty("display_coupon_in_grid") val displayCouponInGrid: Boolean?, // false
        @JsonProperty("display_instant_rebate_grid") val displayInstantRebateGrid: Boolean?, // false
        @JsonProperty("display_killer_deal_in_grid") val displayKillerDealInGrid: Boolean?, // false
        @JsonProperty("display_mail_in_rebate_grid") val displayMailInRebateGrid: Boolean?, // false
        @JsonProperty("document_id") val documentId: String?, // 720884
        @JsonProperty("flags") val flags: List<Flag?>?,
        @JsonProperty("full_name") val fullName: String?, // Trijicon MRO 1x25mm 2 MOA Reticle Red Dot Sight
        @JsonProperty("grid_save_percent") val gridSavePercent: BigDecimal?, // 33
        @JsonProperty("grid_save_percents") val gridSavePercents: GridSavePercents?,
        @JsonProperty("has_free_gifts") val hasFreeGifts: Int?, // 0
        @JsonProperty("has_options") val hasOptions: Int?, // 1
        @JsonProperty("is_best") val isBest: Int?, // 1
        @JsonProperty("is_best_rated") val isBestRated: Int?, // 1
        @JsonProperty("is_call_to_order") val isCallToOrder: Int?, // 0
        @JsonProperty("is_clearance") val isClearance: Int?, // 0
        @JsonProperty("is_demo") val isDemo: Int?, // 0
        @JsonProperty("is_extra_cashback") val isExtraCashback: Boolean?, // false
        @JsonProperty("is_killer_deal") val isKillerDeal: Int?, // 0
        @JsonProperty("is_made_in_usa") val isMadeInUsa: Int?, // 0
        @JsonProperty("is_min_price_after_rebate") val isMinPriceAfterRebate: Boolean?, // false
        @JsonProperty("is_new") val isNew: Int?, // 0
        @JsonProperty("is_orderable") val isOrderable: Int?, // 1
        @JsonProperty("is_outlet") val isOutlet: Int?, // 0
        @JsonProperty("is_rebate") val isRebate: Int?, // 0
        @JsonProperty("is_sale") val isSale: Int?, // 0
        @JsonProperty("is_save_exact") val isSaveExact: Boolean?, // false
        @JsonProperty("is_saving_exact") val isSavingExact: Boolean?, // false
        @JsonProperty("is_second_day_air") val isSecondDayAir: Int?, // 1
        @JsonProperty("is_segment_sale_price_hidden") val isSegmentSalePriceHidden: Int?, // 0
        @JsonProperty("is_shed") val isShed: Int?, // 0
        @JsonProperty("is_use_price_per_uom_calculation") val isUsePricePerUomCalculation: Boolean?, // false
        @JsonProperty("item_flag") val itemFlag: String?, // express
        @JsonProperty("list_price") val listPrice: BigDecimal?, // 579
        @JsonProperty("max_final_sale_price") val maxFinalSalePrice: BigDecimal?, // 539.99
        @JsonProperty("max_final_sale_prices") val maxFinalSalePrices: MaxFinalSalePrices?,
        @JsonProperty("max_final_sale_prices_per_uom_number") val maxFinalSalePricesPerUomNumber: MaxFinalSalePricesPerUomNumber?,
        @JsonProperty("max_grid_saving") val maxGridSaving: BigDecimal?, // 238.01
        @JsonProperty("max_grid_savings") val maxGridSavings: MaxGridSavings?,
        @JsonProperty("max_sale_price") val maxSalePrice: BigDecimal?, // 539.99
        @JsonProperty("max_sale_prices") val maxSalePrices: MaxSalePrices?,
        @JsonProperty("max_saving") val maxSaving: BigDecimal?, // 238.01
        @JsonProperty("max_savings") val maxSavings: MaxSavings?,
        @JsonProperty("min_final_grid_sale_price") val minFinalGridSalePrice: BigDecimal?, // 448.49
        @JsonProperty("min_final_grid_sale_prices") val minFinalGridSalePrices: MinFinalGridSalePrices?,
        @JsonProperty("min_final_sale_price") val minFinalSalePrice: BigDecimal?, // 448.49
        @JsonProperty("min_final_sale_price_uom_labels") val minFinalSalePriceUomLabels: MinFinalSalePriceUomLabels?,
        @JsonProperty("min_final_sale_prices") val minFinalSalePrices: MinFinalSalePrices?,
        @JsonProperty("min_final_sale_prices_per_uom_number") val minFinalSalePricesPerUomNumber: MinFinalSalePricesPerUomNumber?,
        @JsonProperty("min_grid_saving") val minGridSaving: BigDecimal?, // 80.01
        @JsonProperty("min_grid_savings") val minGridSavings: MinGridSavings?,
        @JsonProperty("min_sale_price") val minSalePrice: BigDecimal?, // 448.49
        @JsonProperty("min_sale_prices") val minSalePrices: MinSalePrices?,
        @JsonProperty("min_saving") val minSaving: BigDecimal?, // 80.01
        @JsonProperty("min_savings") val minSavings: MinSavings?,
        @JsonProperty("popularity") val popularity: BigDecimal?, // 99.847
        @JsonProperty("price") val price: BigDecimal?, // 448.49
        @JsonProperty("primary_image") val primaryImage: String?, // opplanet-kinetic-development-group-sidelok-mro-mount-lower-1-3-co-witness-sid5-220-main
        @JsonProperty("product_code") val productCode: String?, // TJ-RD-MRO25
        @JsonProperty("product_id") val productId: Long?, // 720884
        @JsonProperty("promotions") val promotions: Promotions?,
        @JsonProperty("rad_eligible") val radEligible: Boolean?, // false
        @JsonProperty("rebate_amount") val rebateAmount: BigDecimal?, // 0
        @JsonProperty("review_count") val reviewCount: Int?, // 258
        @JsonProperty("review_rating") val reviewRating: String?, // 4.5388
        @JsonProperty("ribbon_flags") val ribbonFlags: List<RibbonFlag?>?,
        @JsonProperty("save_amount") val saveAmount: BigDecimal?, // 238.01
        @JsonProperty("save_percent") val savePercent: BigDecimal?, // 33
        @JsonProperty("save_percents") val savePercents: SavePercents?,
        @JsonProperty("savings_text") val savingsText: String?, // Save Up to 33%
        @JsonProperty("short_name") val shortName: String?, // MRO 1x25mm 2 MOA Reticle Red Dot Sight
        @JsonProperty("show_as_low_as") val showAsLowAs: Boolean?, // true
        @JsonProperty("unorderable_variants") val unorderableVariants: List<UnorderableVariant?>?,
        @JsonProperty("url") val url: String?, // trijicon-1x25mm-mro-2-0-moa-adj-red-dot-sight
        @JsonProperty("variant_count") val variantCount: Int?, // 6
        @JsonProperty("variants") val variants: List<Variant?>?,
        @JsonProperty("whole") val whole: String?, // 448
        @JsonProperty("whole_to") val wholeTo: String? // 539
    ) {
        data class AreMinPricesAfterRebate @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: Boolean?, // false
            @JsonProperty("offhours") val offhours: Boolean?, // false
            @JsonProperty("regular") val regular: Boolean?, // false
            @JsonProperty("test_evasive") val testEvasive: Boolean?, // false
            @JsonProperty("test_offhours") val testOffhours: Boolean?, // false
            @JsonProperty("test_regular") val testRegular: Boolean? // false
        )

        data class AreSavingsExact @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: Boolean?, // false
            @JsonProperty("offhours") val offhours: Boolean?, // false
            @JsonProperty("regular") val regular: Boolean?, // false
            @JsonProperty("test_evasive") val testEvasive: Boolean?, // false
            @JsonProperty("test_offhours") val testOffhours: Boolean?, // false
            @JsonProperty("test_regular") val testRegular: Boolean? // false
        )

        data class AreSegmentSalePricesHidden @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 0
            @JsonProperty("offhours") val offhours: BigDecimal?, // 0
            @JsonProperty("regular") val regular: BigDecimal?, // 0
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 0
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 0
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 0
        )

        data class Brand @JsonCreator constructor(
            @JsonProperty("facet") val facet: String?, // Trijicon:===:70:===:trijicon-brand:===:slug=trijicon:===:featured=1
            @JsonProperty("id") val id: Int?, // 70
            @JsonProperty("images") val images: List<Image?>?,
            @JsonProperty("inverted_popularity") val invertedPopularity: BigDecimal?, // 156.0000000000059
            @JsonProperty("name") val name: String?, // Trijicon
            @JsonProperty("popularity") val popularity: String?, // 99.844
            @JsonProperty("slug") val slug: String?, // trijicon
            @JsonProperty("url") val url: String? // trijicon-brand
        ) {
            data class Image @JsonCreator constructor(
                @JsonProperty("source") val source: String?, // brand
                @JsonProperty("title") val title: String?, // Trijicon Brand Logo 2014
                @JsonProperty("url") val url: String? // opplanet-trijicon-brand-logo-2014
            )
        }

        data class Category @JsonCreator constructor(
            @JsonProperty("facet") val facet: String?, // Red Dot Sights:===:20:===:red-dot-scopes:===:slug=red-dot-scopes:===:featured=1
            @JsonProperty("id") val id: String?, // 20
            @JsonProperty("inverted_popularity") val invertedPopularity: BigDecimal?, // 683.0000000000069
            @JsonProperty("lev") val lev: String?, // 3
            @JsonProperty("name") val name: String?, // Red Dot Sights
            @JsonProperty("parent_id") val parentId: String?, // 4432
            @JsonProperty("popularity") val popularity: String?, // 99.317
            @JsonProperty("product_count") val productCount: Int?, // 648
            @JsonProperty("slug") val slug: String?, // red-dot-scopes
            @JsonProperty("url") val url: String? // red-dot-scopes
        )

        data class Flag @JsonCreator constructor(
            @JsonProperty("facet") val facet: String?, // Demo:===::===:demo
            @JsonProperty("name") val name: String?, // Demo
            @JsonProperty("url") val url: String? // demo
        )

        data class GridSavePercents @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 33
            @JsonProperty("offhours") val offhours: BigDecimal?, // 33
            @JsonProperty("regular") val regular: BigDecimal?, // 33
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 33
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 33
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 33
        )

        data class MaxFinalSalePrices @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 539.99
            @JsonProperty("offhours") val offhours: BigDecimal?, // 539.99
            @JsonProperty("regular") val regular: BigDecimal?, // 539.99
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 539.99
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 539.99
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 539.99
        )

        data class MaxFinalSalePricesPerUomNumber @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 539.99
            @JsonProperty("offhours") val offhours: BigDecimal?, // 539.99
            @JsonProperty("regular") val regular: BigDecimal?, // 539.99
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 539.99
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 539.99
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 539.99
        )

        data class MaxGridSavings @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 238.01
            @JsonProperty("offhours") val offhours: BigDecimal?, // 238.01
            @JsonProperty("regular") val regular: BigDecimal?, // 238.01
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 238.01
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 238.01
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 238.01
        )

        data class MaxSalePrices @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 539.99
            @JsonProperty("offhours") val offhours: BigDecimal?, // 539.99
            @JsonProperty("regular") val regular: BigDecimal?, // 539.99
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 539.99
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 539.99
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 539.99
        )

        data class MaxSavings @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 238.01
            @JsonProperty("offhours") val offhours: BigDecimal?, // 238.01
            @JsonProperty("regular") val regular: BigDecimal?, // 238.01
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 238.01
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 238.01
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 238.01
        )

        data class MinFinalGridSalePrices @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 448.49
            @JsonProperty("offhours") val offhours: BigDecimal?, // 448.49
            @JsonProperty("regular") val regular: BigDecimal?, // 448.49
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 448.49
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 448.49
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 448.49
        )

        data class MinFinalSalePriceUomLabels @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: String?,
            @JsonProperty("offhours") val offhours: String?,
            @JsonProperty("regular") val regular: String?,
            @JsonProperty("test_evasive") val testEvasive: String?,
            @JsonProperty("test_offhours") val testOffhours: String?,
            @JsonProperty("test_regular") val testRegular: String?
        )

        data class MinFinalSalePrices @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 448.49
            @JsonProperty("offhours") val offhours: BigDecimal?, // 448.49
            @JsonProperty("regular") val regular: BigDecimal?, // 448.49
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 448.49
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 448.49
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 448.49
        )

        data class MinFinalSalePricesPerUomNumber @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 448.49
            @JsonProperty("offhours") val offhours: BigDecimal?, // 448.49
            @JsonProperty("regular") val regular: BigDecimal?, // 448.49
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 448.49
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 448.49
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 448.49
        )

        data class MinGridSavings @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 80.01
            @JsonProperty("offhours") val offhours: BigDecimal?, // 80.01
            @JsonProperty("regular") val regular: BigDecimal?, // 80.01
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 80.01
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 80.01
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 80.01
        )

        data class MinSalePrices @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 448.49
            @JsonProperty("offhours") val offhours: BigDecimal?, // 448.49
            @JsonProperty("regular") val regular: BigDecimal?, // 448.49
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 448.49
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 448.49
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 448.49
        )

        data class MinSavings @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 80.01
            @JsonProperty("offhours") val offhours: BigDecimal?, // 80.01
            @JsonProperty("regular") val regular: BigDecimal?, // 80.01
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 80.01
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 80.01
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 80.01
        )

        data class Promotions @JsonCreator constructor(
            @JsonProperty("marketing") val marketing: List<Marketing?>?
        ) {
            data class Marketing @JsonCreator constructor(
                @JsonProperty("end") val end: String?, // 2021-07-01 00:00:00
                @JsonProperty("is_hidden") val isHidden: Boolean?, // false
                @JsonProperty("on_product_grid") val onProductGrid: Int?, // 1
                @JsonProperty("on_product_page") val onProductPage: Int?, // 1
                @JsonProperty("on_promo_page") val onPromoPage: Int?, // 1
                @JsonProperty("on_promotion_page") val onPromotionPage: Int?, // 1
                @JsonProperty("promotion_id") val promotionId: Long?, // 85749
                @JsonProperty("start") val start: String?, // 2021-04-01 00:00:00
                @JsonProperty("type") val type: String? // MK
            )
        }

        data class RibbonFlag @JsonCreator constructor(
            @JsonProperty("item_flag") val itemFlag: String?, // express
            @JsonProperty("price_flag") val priceFlag: String?, // Free 2 Day Shipping
            @JsonProperty("priority") val priority: String? // medium-priority
        )

        data class SavePercents @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 33
            @JsonProperty("offhours") val offhours: BigDecimal?, // 33
            @JsonProperty("regular") val regular: BigDecimal?, // 33
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 33
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 33
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 33
        )

        data class UnorderableVariant @JsonCreator constructor(
            @JsonProperty("document_id") val documentId: String?, // 2370507
            @JsonProperty("has_filterable_specifications") val hasFilterableSpecifications: Boolean?, // false
            @JsonProperty("is_orderable") val isOrderable: String?, // 0
            @JsonProperty("product_id") val productId: String? // 720884
        )

        data class Variant @JsonCreator constructor(
            @JsonProperty("document_id") val documentId: String?, // 1418607
            @JsonProperty("has_filterable_specifications") val hasFilterableSpecifications: Boolean?, // false
            @JsonProperty("is_orderable") val isOrderable: String?, // 1
            @JsonProperty("product_id") val productId: String?, // 720884
            @JsonProperty("stock_availability") val stockAvailability: StockAvailability?
        ) {
            data class StockAvailability @JsonCreator constructor(
                @JsonProperty("availability") val availability: String?, // Estimated to ship within 7-18 days
                @JsonProperty("max_days_to_ship") val maxDaysToShip: Int?, // 18
                @JsonProperty("qoh") val qoh: Int? // 0
            )
        }
    }

    data class SpecialOffers @JsonCreator constructor(
        @JsonProperty("category") val category: Category?,
        @JsonProperty("is_best_rated") val isBestRated: Int?, // 0
        @JsonProperty("is_clearance") val isClearance: Int?, // 0
        @JsonProperty("is_coupon") val isCoupon: Int?, // 0
        @JsonProperty("is_extra_cashback") val isExtraCashback: Int?, // 0
        @JsonProperty("is_free_gift") val isFreeGift: Int?, // 0
        @JsonProperty("is_instant_rebate") val isInstantRebate: Int?, // 0
        @JsonProperty("is_killer_deal") val isKillerDeal: Int?, // 0
        @JsonProperty("is_mail_in_rebate") val isMailInRebate: Int?, // 0
        @JsonProperty("is_new") val isNew: Int?, // 0
        @JsonProperty("is_outlet") val isOutlet: Int?, // 0
        @JsonProperty("is_sale") val isSale: Int?, // 0
        @JsonProperty("is_seasonal") val isSeasonal: Int?, // 0
        @JsonProperty("is_second_day_air") val isSecondDayAir: Int?, // 0
        @JsonProperty("is_shed") val isShed: Int?, // 0
        @JsonProperty("primary") val primary: String?, // plain
        @JsonProperty("special_offer_to_type") val specialOfferToType: SpecialOfferToType?
    ) {
        data class Category @JsonCreator constructor(
            @JsonProperty("facet") val facet: String?, // Auto & Atv:===:3445:===:auto-atv:===:slug=auto-atv:===:featured=0
            @JsonProperty("id") val id: String?, // 3445
            @JsonProperty("inverted_popularity") val invertedPopularity: BigDecimal?, // 12608.000000000004
            @JsonProperty("lev") val lev: String?, // 2
            @JsonProperty("name") val name: String?, // Auto & Atv
            @JsonProperty("parent_id") val parentId: String?, // 1
            @JsonProperty("popularity") val popularity: String?, // 87.392
            @JsonProperty("product_count") val productCount: Int?, // 1161
            @JsonProperty("slug") val slug: String?, // auto-atv
            @JsonProperty("url") val url: String? // auto-atv
        )

        data class SpecialOfferToType @JsonCreator constructor(
            @JsonProperty("is_best_rated") val isBestRated: String?, // best_rated
            @JsonProperty("is_clearance") val isClearance: String?, // clearance
            @JsonProperty("is_coupon") val isCoupon: String?, // coupon
            @JsonProperty("is_extra_cashback") val isExtraCashback: String?, // extra_cashback
            @JsonProperty("is_final_sale") val isFinalSale: String?, // final_sale
            @JsonProperty("is_free_gift") val isFreeGift: String?, // free_gift
            @JsonProperty("is_instant_rebate") val isInstantRebate: String?, // instant_rebate
            @JsonProperty("is_killer_deal") val isKillerDeal: String?, // killer_deal
            @JsonProperty("is_mail_in_rebate") val isMailInRebate: String?, // mail_in_rebate
            @JsonProperty("is_new") val isNew: String?, // new
            @JsonProperty("is_sale") val isSale: String?, // sale
            @JsonProperty("is_seasonal") val isSeasonal: String?, // marketing
            @JsonProperty("is_second_day_air") val isSecondDayAir: String? // second_day_air
        )
    }

    data class Variant @JsonCreator constructor(
        @JsonProperty("add_to_cart_text") val addToCartText: String?, // Add to Cart
        @JsonProperty("answer_count") val answerCount: Int?, // 0
        @JsonProperty("are_segment_sale_prices_hidden") val areSegmentSalePricesHidden: AreSegmentSalePricesHidden?,
        @JsonProperty("availability_message") val availabilityMessage: String?, // 4 left, order now! Ships in 1-4 days.
        @JsonProperty("brand") val brand: Brand?,
        @JsonProperty("brand_categories") val brandCategories: List<BrandCategory?>?,
        @JsonProperty("brand_id") val brandId: Long?, // 0
        @JsonProperty("breadcrumbs") val breadcrumbs: List<List<Breadcrumb?>?>?,
        @JsonProperty("bundle_data") val bundleData: BundleData?,
        @JsonProperty("cashback") val cashback: BigDecimal?, // 0.44
        @JsonProperty("cashback_percentage") val cashbackPercentage: BigDecimal?, // 0
        @JsonProperty("categories") val categories: List<Category?>?,
        @JsonProperty("clean_description") val cleanDescription: String?, // Add some extra comfort and protection to your ride when you snap a Comfort Ridr Seat Protector to your ATV. The versatile gear does two jobs on any terrain. The first you'll notice is the soft-molded foam. It adds extra cushion for those rough rides. The seat protector also acts as a cover, stopping existing rips and tears from growing on the seat underneath. It's easy to attach. Just snap the three buckles in place for a secure fit.Features/Specifications:Soft molded foam adds extra cushion, Covers existing rips and tears and stops them from growing bigger, Three quick release buckles easily secure protector to the seat, Versatile doing two jobs on any terrain, Easy to attach by just snapping the three buckles in place
        @JsonProperty("code") val code: String?, // 2GZ-AA-2000012623
        @JsonProperty("departments") val departments: List<Int?>?,
        @JsonProperty("description") val description: String?, // <p>Add some extra comfort and protection to your ride when you snap a Comfort Ridr Seat Protector to your ATV. The versatile gear does two jobs on any terrain. The first you'll notice is the soft-molded foam. It adds extra cushion for those rough rides. The seat protector also acts as a cover, stopping existing rips and tears from growing on the seat underneath. It's easy to attach. Just snap the three buckles in place for a secure fit.</p><p><strong>Features/Specifications:</strong></p><p>Soft molded foam adds extra cushion, Covers existing rips and tears and stops them from growing bigger, Three quick release buckles easily secure protector to the seat, Versatile doing two jobs on any terrain, Easy to attach by just snapping the three buckles in place</p>
        @JsonProperty("document_id") val documentId: String?, // 1524400
        @JsonProperty("estimated_cost") val estimatedCost: BigDecimal?, // 13.35
        @JsonProperty("final_grid_sale_price") val finalGridSalePrice: BigDecimal?, // 21.99
        @JsonProperty("final_grid_sale_prices") val finalGridSalePrices: FinalGridSalePrices?,
        @JsonProperty("final_sale_price") val finalSalePrice: BigDecimal?, // 21.99
        @JsonProperty("final_sale_prices") val finalSalePrices: FinalSalePrices?,
        @JsonProperty("final_sale_prices_per_uom_number") val finalSalePricesPerUomNumber: FinalSalePricesPerUomNumber?,
        @JsonProperty("free_shipping") val freeShipping: Int?, // 0
        @JsonProperty("full_name") val fullName: String?, // Mad Dog ATV Comfort Ride Seat Protector, 3 Quick Release Buckles, Black 2000012623
        @JsonProperty("gift_certificate") val giftCertificate: Int?, // 0
        @JsonProperty("grid_save_percents") val gridSavePercents: GridSavePercents?,
        @JsonProperty("grid_savings") val gridSavings: GridSavings?,
        @JsonProperty("gtin") val gtin: String?, // 0076501065763
        @JsonProperty("has_coupons") val hasCoupons: Int?, // 0
        @JsonProperty("has_filterable_specifications") val hasFilterableSpecifications: Boolean?, // false
        @JsonProperty("has_free_gifts") val hasFreeGifts: Int?, // 0
        @JsonProperty("has_manual_description") val hasManualDescription: String?, // 1
        @JsonProperty("has_override_price") val hasOverridePrice: String?, // 0
        @JsonProperty("image") val image: String?, // opplanet-mad-dog-atv-comfort-ride-seat-protector-2000012623-main
        @JsonProperty("inverted_popularity") val invertedPopularity: BigDecimal?, // 50680
        @JsonProperty("is_available_on_op") val isAvailableOnOp: String?, // 0
        @JsonProperty("is_best") val isBest: Int?, // 1
        @JsonProperty("is_best_rated") val isBestRated: Int?, // 0
        @JsonProperty("is_best_sale") val isBestSale: Int?, // 0
        @JsonProperty("is_call_to_order") val isCallToOrder: Int?, // 0
        @JsonProperty("is_clearance") val isClearance: Int?, // 0
        @JsonProperty("is_demo") val isDemo: Int?, // 0
        @JsonProperty("is_extra_cashback") val isExtraCashback: Boolean?, // false
        @JsonProperty("is_killer_deal") val isKillerDeal: Int?, // 0
        @JsonProperty("is_kit") val isKit: String?, // 0
        @JsonProperty("is_made_in_usa") val isMadeInUsa: String?, // 0
        @JsonProperty("is_new") val isNew: Int?, // 0
        @JsonProperty("is_orderable") val isOrderable: String?, // 1
        @JsonProperty("is_outlet") val isOutlet: Int?, // 0
        @JsonProperty("is_price_after_rebate") val isPriceAfterRebate: Boolean?, // false
        @JsonProperty("is_rebate") val isRebate: Int?, // 0
        @JsonProperty("is_refurb") val isRefurb: Boolean?, // false
        @JsonProperty("is_sale") val isSale: Int?, // 0
        @JsonProperty("is_second_day_air") val isSecondDayAir: Int?, // 0
        @JsonProperty("is_segment_sale_price_hidden") val isSegmentSalePriceHidden: Int?, // 0
        @JsonProperty("is_shed") val isShed: Int?, // 0
        @JsonProperty("is_test") val isTest: String?, // 0
        @JsonProperty("lowest_prices") val lowestPrices: LowestPrices?,
        @JsonProperty("manufacturer_code") val manufacturerCode: String?, // 2000012623
        @JsonProperty("msrp") val msrp: String?, // 23.99
        @JsonProperty("name") val name: String?, // Mad Dog ATV Comfort Ride Seat Protector, 3 Quick Release Buckles, Black 2000012623
        @JsonProperty("op_sale_price") val opSalePrice: String?, // 0.00
        @JsonProperty("popularity") val popularity: String?, // 49.32
        @JsonProperty("price") val price: String?, // 23.99
        @JsonProperty("price_per_uom") val pricePerUom: PricePerUom?,
        @JsonProperty("product_anchor_text") val productAnchorText: String?, // Mad Dog ATV Comfort Ride Seat Protector
        @JsonProperty("product_clean_description") val productCleanDescription: String?, // Add some extra comfort and protection to your ride when you snap a Comfort Ride Seat Protector to your ATV. The versatile gear does two jobs on any terrain. The first you'll notice is the soft-molded foam. It adds extra cushion for those rough rides. The seat protector also acts as a cover, stopping existing rips and tears from growing on the seat underneath. It's easy to attach. Just snap the three buckles in place for a secure fit. Soft molded foam adds extra cushion. Covers existing rips and tears and stops them from growing bigger. Three quick release buckles easily secure protector to the seat. Versatile doing two jobs on any terrain. Easy to attach by just snapping the three buckles in place.Package Contents:Mad Dog ATV Comfort Ride Seat ProtectorMad Dog 2000012623: ATV Comfort Ride Seat Protector, 3 Quick Release Buckles, Black
        @JsonProperty("product_code") val productCode: String?, // 2GZ-AA-2000012623
        @JsonProperty("product_full_name") val productFullName: String?, // Mad Dog ATV Comfort Ride Seat Protector
        @JsonProperty("product_id") val productId: String?, // 773046
        @JsonProperty("product_image") val productImage: String?, // opplanet-mad-dog-atv-comfort-ride-seat-protector-2000012623-main
        @JsonProperty("product_short_name") val productShortName: String?, // ATV Comfort Ride Seat Protector
        @JsonProperty("product_url") val productUrl: String?, // mad-dog-atv-comfort-ride-seat-protector
        @JsonProperty("prop65_chemicals") val prop65Chemicals: Prop65Chemicals?,
        @JsonProperty("qoh") val qoh: String?, // 4
        @JsonProperty("question_count") val questionCount: Int?, // 2
        @JsonProperty("rad_eligibles") val radEligibles: RadEligibles?,
        @JsonProperty("related_info_pages") val relatedInfoPages: List<String?>?,
        @JsonProperty("relations") val relations: Int?, // 0
        @JsonProperty("review_count") val reviewCount: Int?, // 0
        @JsonProperty("review_rating") val reviewRating: String?, // 0
        @JsonProperty("review_rating_weighted") val reviewRatingWeighted: String?, // 0
        @JsonProperty("sale") val sale2: BigDecimal?, // 21.99
        @JsonProperty("sale_prices") val salePrices: SalePrices?,
        @JsonProperty("save_percents") val savePercents: SavePercents?,
        @JsonProperty("savings") val savings: Savings?,
        @JsonProperty("sku") val sku: String?, // 2GZ-AA-2000012623
        @JsonProperty("special_offers") val specialOffers: SpecialOffers?,
        @JsonProperty("stock_availability") val stockAvailability: StockAvailability?,
        @JsonProperty("tax_relief_discount") val taxReliefDiscount: TaxReliefDiscount?,
        @JsonProperty("total_variant_count") val totalVariantCount: Int?, // 1
        @JsonProperty("unorderable_variant_count") val unorderableVariantCount: Int?, // 0
        @JsonProperty("upc") val upc: String?, // 076501065763
        @JsonProperty("variant_count") val variantCount: Int?, // 1
        @JsonProperty("variant_flags") val variantFlags: List<String?>?,
        @JsonProperty("variant_id") val variantId: String?, // 1524400
        @JsonProperty("variant_sku") val variantSku: String?, // 2GZ-AA-2000012623
        @JsonProperty("weight") val weight: String?, // 1
        @JsonProperty("yad") val yad: String? // 49.32
    ) {
        data class AreSegmentSalePricesHidden @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 0
            @JsonProperty("offhours") val offhours: BigDecimal?, // 0
            @JsonProperty("regular") val regular: BigDecimal?, // 0
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 0
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 0
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 0
        )

        data class Brand @JsonCreator constructor(
            @JsonProperty("facet") val facet: String?, // MadDog:===:6070:===:mad-dog-brand:===:slug=maddog:===:featured=0
            @JsonProperty("id") val id: Long?, // 6070
            @JsonProperty("images") val images: List<Image?>?,
            @JsonProperty("inverted_popularity") val invertedPopularity: BigDecimal?, // 42406
            @JsonProperty("name") val name: String?, // MadDog
            @JsonProperty("popularity") val popularity: String?, // 57.594
            @JsonProperty("slug") val slug: String?, // maddog
            @JsonProperty("url") val url: String? // mad-dog-brand
        ) {
            data class Image @JsonCreator constructor(
                @JsonProperty("source") val source: String?, // brand
                @JsonProperty("title") val title: String?, // MadDog 2019 Logo
                @JsonProperty("url") val url: String? // opplanet-maddog-2019-logo
            )
        }

        data class BrandCategory @JsonCreator constructor(
            @JsonProperty("brand_id") val brandId: String?, // 6070
            @JsonProperty("brand_name") val brandName: String?, // MadDog
            @JsonProperty("category_id") val categoryId: String?, // 3
            @JsonProperty("category_name") val categoryName: String?, // ATV Box Accessories
            @JsonProperty("facet") val facet: String?, // Mad Dog ATV Box Accessories:===:177256:===:mad-dog-atv-boxes-accessories:===:full_name=Mad Dog ATV Box Accessories:===:brand_name=MadDog:===:brand_full_name=MadDog:===:category_name=ATV Box Accessories
            @JsonProperty("id") val id: String?, // 177256
            @JsonProperty("inverted_popularity") val invertedPopularity: BigDecimal?, // 58500
            @JsonProperty("name") val name: String?, // Mad Dog ATV Box Accessories
            @JsonProperty("orderable_count") val orderableCount: String?, // 7
            @JsonProperty("parent_category_id") val parentCategoryId: String?, // 72
            @JsonProperty("popularity") val popularity: BigDecimal?, // 41.5
            @JsonProperty("url") val url: String? // mad-dog-atv-boxes-accessories
        )

        data class Breadcrumb @JsonCreator constructor(
            @JsonProperty("separator") val separator: String?, // arrow
            @JsonProperty("title") val title: String?, // MadDog
            @JsonProperty("url") val url: String? // mad-dog-brand
        )

        data class BundleData @JsonCreator constructor(
            @JsonProperty("bundle") val bundle: List<Bundle?>?,
            @JsonProperty("bundleEndPriceVariantData") val bundleEndPriceVariantData: List<BundleEndPriceVariantData?>?,
            @JsonProperty("bundleMaxPrice") val bundleMaxPrice: BigDecimal?, // 23.99
            @JsonProperty("bundleMessage") val bundleMessage: String?,
            @JsonProperty("bundleStartPrice") val bundleStartPrice: BigDecimal?, // 21.99
            @JsonProperty("hasTooLowToShow") val hasTooLowToShow: Boolean?, // false
            @JsonProperty("maxCashback") val maxCashback: BigDecimal?, // 0.44
            @JsonProperty("maxVariantSaveOriginal") val maxVariantSaveOriginal: String?, // 23.99
            @JsonProperty("maxVariantSaveSale") val maxVariantSaveSale: BigDecimal?, // 21.99
            @JsonProperty("savePercent") val savePercent: BigDecimal? // 8
        ) {
            data class Bundle @JsonCreator constructor(
                @JsonProperty("brand_name") val brandName: String?,
                @JsonProperty("bundle_id") val bundleId: String?,
                @JsonProperty("cashback") val cashback: BigDecimal?, // 0.44
                @JsonProperty("cashback_percentage") val cashbackPercentage: BigDecimal?, // 0
                @JsonProperty("category_name") val categoryName: String?,
                @JsonProperty("full_name") val fullName: String?,
                @JsonProperty("group_label") val groupLabel: String?,
                @JsonProperty("has_coupons") val hasCoupons: Boolean?, // false
                @JsonProperty("has_promos") val hasPromos: Boolean?, // false
                @JsonProperty("image") val image: String?, // opplanet-mad-dog-atv-comfort-ride-seat-protector-2000012623-main
                @JsonProperty("is_disabled") val isDisabled: Boolean?, // true
                @JsonProperty("is_extra_cashback") val isExtraCashback: Boolean?, // false
                @JsonProperty("is_free") val isFree: Boolean?, // false
                @JsonProperty("is_second_day_air") val isSecondDayAir: Int?, // 0
                @JsonProperty("original_price") val originalPrice: String?, // 23.99
                @JsonProperty("price") val price: BigDecimal?, // 21.99
                @JsonProperty("price_prefix") val pricePrefix: String?,
                @JsonProperty("product_code") val productCode: String?,
                @JsonProperty("sku") val sku: String?, // 2GZ-AA-2000012623
                @JsonProperty("variant_id") val variantId: String?, // 1524400
                @JsonProperty("variant_name") val variantName: String? // Mad Dog ATV Comfort Ride Seat Protector, 3 Quick Release Buckles, Black 2000012623
            )

            data class BundleEndPriceVariantData @JsonCreator constructor(
                @JsonProperty("price") val price: BigDecimal?, // 21.99
                @JsonProperty("variant_id") val variantId: String? // 1524400
            )
        }

        data class Category @JsonCreator constructor(
            @JsonProperty("facet") val facet: String?, // ATV Box Accessories:===:3:===:atv-boxes-accessories:===:slug=atv-boxes-accessories:===:featured=0
            @JsonProperty("id") val id: String?, // 3
            @JsonProperty("inverted_popularity") val invertedPopularity: BigDecimal?, // 21393
            @JsonProperty("lev") val lev: String?, // 3
            @JsonProperty("name") val name: String?, // ATV Box Accessories
            @JsonProperty("parent_id") val parentId: String?, // 72
            @JsonProperty("popularity") val popularity: String?, // 78.607
            @JsonProperty("product_count") val productCount: Int?, // 7
            @JsonProperty("slug") val slug: String?, // atv-boxes-accessories
            @JsonProperty("url") val url: String? // atv-boxes-accessories
        )

        data class FinalGridSalePrices @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 21.99
            @JsonProperty("offhours") val offhours: BigDecimal?, // 21.99
            @JsonProperty("regular") val regular: BigDecimal?, // 21.99
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 21.99
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 21.99
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 21.99
        )

        data class FinalSalePrices @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 21.99
            @JsonProperty("offhours") val offhours: BigDecimal?, // 21.99
            @JsonProperty("regular") val regular: BigDecimal?, // 21.99
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 21.99
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 21.99
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 21.99
        )

        data class FinalSalePricesPerUomNumber @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 21.99
            @JsonProperty("offhours") val offhours: BigDecimal?, // 21.99
            @JsonProperty("regular") val regular: BigDecimal?, // 21.99
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 21.99
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 21.99
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 21.99
        )

        data class GridSavePercents @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 8
            @JsonProperty("offhours") val offhours: BigDecimal?, // 8
            @JsonProperty("regular") val regular: BigDecimal?, // 8
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 8
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 8
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 8
        )

        data class GridSavings @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 2
            @JsonProperty("offhours") val offhours: BigDecimal?, // 2
            @JsonProperty("regular") val regular: BigDecimal?, // 2
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 2
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 2
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 2
        )

        data class LowestPrices @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 1
            @JsonProperty("offhours") val offhours: BigDecimal?, // 1
            @JsonProperty("regular") val regular: BigDecimal?, // 1
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 1
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 1
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 1
        )

        data class PricePerUom @JsonCreator constructor(
            @JsonProperty("is_use_price_per_uom_calculation") val isUsePricePerUomCalculation: Boolean? // false
        )

        data class Prop65Chemicals @JsonCreator constructor(
            @JsonProperty("both") val both: List<String?>?
        )

        data class RadEligibles @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: Boolean?, // false
            @JsonProperty("offhours") val offhours: Boolean?, // false
            @JsonProperty("regular") val regular: Boolean?, // false
            @JsonProperty("test_evasive") val testEvasive: Boolean?, // false
            @JsonProperty("test_offhours") val testOffhours: Boolean?, // false
            @JsonProperty("test_regular") val testRegular: Boolean? // false
        )

        data class SalePrices @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: String?, // 21.99
            @JsonProperty("offhours") val offhours: String?, // 21.99
            @JsonProperty("regular") val regular: String?, // 21.99
            @JsonProperty("test_evasive") val testEvasive: String?, // 21.99
            @JsonProperty("test_offhours") val testOffhours: String?, // 21.99
            @JsonProperty("test_regular") val testRegular: String? // 21.99
        )

        data class SavePercents @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 8
            @JsonProperty("offhours") val offhours: BigDecimal?, // 8
            @JsonProperty("regular") val regular: BigDecimal?, // 8
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 8
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 8
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 8
        )

        data class Savings @JsonCreator constructor(
            @JsonProperty("evasive") val evasive: BigDecimal?, // 2
            @JsonProperty("offhours") val offhours: BigDecimal?, // 2
            @JsonProperty("regular") val regular: BigDecimal?, // 2
            @JsonProperty("test_evasive") val testEvasive: BigDecimal?, // 2
            @JsonProperty("test_offhours") val testOffhours: BigDecimal?, // 2
            @JsonProperty("test_regular") val testRegular: BigDecimal? // 2
        )

        data class SpecialOffers @JsonCreator constructor(
            @JsonProperty("category") val category: Category?,
            @JsonProperty("is_best_rated") val isBestRated: Int?, // 0
            @JsonProperty("is_clearance") val isClearance: Int?, // 0
            @JsonProperty("is_coupon") val isCoupon: Int?, // 0
            @JsonProperty("is_extra_cashback") val isExtraCashback: Int?, // 0
            @JsonProperty("is_free_gift") val isFreeGift: Int?, // 0
            @JsonProperty("is_instant_rebate") val isInstantRebate: Int?, // 0
            @JsonProperty("is_killer_deal") val isKillerDeal: Int?, // 0
            @JsonProperty("is_mail_in_rebate") val isMailInRebate: Int?, // 0
            @JsonProperty("is_new") val isNew: Int?, // 0
            @JsonProperty("is_outlet") val isOutlet: Int?, // 0
            @JsonProperty("is_sale") val isSale: Int?, // 0
            @JsonProperty("is_seasonal") val isSeasonal: Int?, // 0
            @JsonProperty("is_second_day_air") val isSecondDayAir: Int?, // 0
            @JsonProperty("is_shed") val isShed: Int?, // 0
            @JsonProperty("primary") val primary: String?, // plain
            @JsonProperty("special_offer_to_type") val specialOfferToType: SpecialOfferToType?
        ) {
            data class Category @JsonCreator constructor(
                @JsonProperty("facet") val facet: String?, // Auto & Atv:===:3445:===:auto-atv:===:slug=auto-atv:===:featured=0
                @JsonProperty("id") val id: String?, // 3445
                @JsonProperty("inverted_popularity") val invertedPopularity: BigDecimal?, // 12608.000000000004
                @JsonProperty("lev") val lev: String?, // 2
                @JsonProperty("name") val name: String?, // Auto & Atv
                @JsonProperty("parent_id") val parentId: String?, // 1
                @JsonProperty("popularity") val popularity: String?, // 87.392
                @JsonProperty("product_count") val productCount: Int?, // 1161
                @JsonProperty("slug") val slug: String?, // auto-atv
                @JsonProperty("url") val url: String? // auto-atv
            )

            data class SpecialOfferToType @JsonCreator constructor(
                @JsonProperty("is_best_rated") val isBestRated: String?, // best_rated
                @JsonProperty("is_clearance") val isClearance: String?, // clearance
                @JsonProperty("is_coupon") val isCoupon: String?, // coupon
                @JsonProperty("is_extra_cashback") val isExtraCashback: String?, // extra_cashback
                @JsonProperty("is_final_sale") val isFinalSale: String?, // final_sale
                @JsonProperty("is_free_gift") val isFreeGift: String?, // free_gift
                @JsonProperty("is_instant_rebate") val isInstantRebate: String?, // instant_rebate
                @JsonProperty("is_killer_deal") val isKillerDeal: String?, // killer_deal
                @JsonProperty("is_mail_in_rebate") val isMailInRebate: String?, // mail_in_rebate
                @JsonProperty("is_new") val isNew: String?, // new
                @JsonProperty("is_sale") val isSale: String?, // sale
                @JsonProperty("is_seasonal") val isSeasonal: String?, // marketing
                @JsonProperty("is_second_day_air") val isSecondDayAir: String? // second_day_air
            )
        }

        data class StockAvailability @JsonCreator constructor(
            @JsonProperty("availability") val availability: String?, // 4 left, order now! Ships in 1-4 days.
            @JsonProperty("expected_date_max") val expectedDateMax: String?, // 2021-04-28
            @JsonProperty("expected_date_min") val expectedDateMin: String?, // 2021-04-26
            @JsonProperty("generated_at") val generatedAt: String?, // 2021-04-24T01:34:26-05:00
            @JsonProperty("generated_on") val generatedOn: String?, // 2021-04-24
            @JsonProperty("is_dropshippable_by_primary_supplier") val isDropshippableByPrimarySupplier: Boolean?, // false
            @JsonProperty("max_days_to_ship") val maxDaysToShip: Int?, // 3
            @JsonProperty("min_days_to_ship") val minDaysToShip: Int?, // 1
            @JsonProperty("qoh") val qoh: Int?, // 4
            @JsonProperty("remaining_qoh") val remainingQoh: Int?, // 4
            @JsonProperty("use_business_days_only") val useBusinessDaysOnly: Boolean?, // true
            @JsonProperty("version") val version: Long? // 29375442820
        )

        data class TaxReliefDiscount @JsonCreator constructor(
            @JsonProperty("is_excluded_for_coupons") val isExcludedForCoupons: Int? // 0
        )
    }
}