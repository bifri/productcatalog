package io.bifri.productcatalog.network.productsservice

import io.bifri.productcatalog.network.response.categories.Category
import io.bifri.productcatalog.network.response.product.Product
import io.bifri.productcatalog.network.response.products.ProductsResponse
import io.bifri.productcatalog.util.HEADER_ACCEPT_APPLICATION_JSON
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

private const val IV_INCLUDE_PARAM_VALUE = "gridProducts"

interface ProductsService {

    @Headers(HEADER_ACCEPT_APPLICATION_JSON)
    @GET("api/0.5/categories") suspend fun categories(): List<Category?>

    @GET("iv-api/0.5/catalog/{identifier}/products") suspend fun products(
        @Path("identifier") id: String,
        @Query("_iv_include") ivInclude: String = IV_INCLUDE_PARAM_VALUE
    ): ProductsResponse

    @GET("api/0.5/products/{identifier}") suspend fun product(
        @Path("identifier") id: String
    ): Product

}