package io.bifri.productcatalog.base.activity

import android.os.Bundle
import io.bifri.productcatalog.app.App
import io.bifri.productcatalog.base.viewmodel.RxViewBinder

abstract class BaseActivity : AppCompatLifecycleLoggingActivity(), HasActivityComponent {

    abstract val viewBinder: RxViewBinder
    private var _activityComponent: Any? = null
    override val activityComponent get() = _activityComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
        viewBinder.bind(this)
    }

    private fun inject() {
        check(_activityComponent == null) { "Activity must not use Injector more than once" }
        val componentInjector = (application as App).activityComponentInjector(this)
        _activityComponent = componentInjector.component
        componentInjector.injector.inject(this)
    }

}