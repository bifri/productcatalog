package io.bifri.productcatalog.base.activity

interface HasActivityComponent {
    val activityComponent: Any?
}