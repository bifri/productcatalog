package io.bifri.productcatalog.base.fragment

abstract class NoArgsBaseFragment<VIEW_STATE> : BaseFragment<Unit, VIEW_STATE>() {

    override val args = Unit

}