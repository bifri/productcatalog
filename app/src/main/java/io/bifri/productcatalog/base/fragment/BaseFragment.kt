package io.bifri.productcatalog.base.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import io.bifri.productcatalog.R
import io.bifri.productcatalog.exception.SimpleErrorDialogGetter
import io.bifri.productcatalog.ui.dialog.simpleerror.ErrorFragmentArgs
import io.bifri.productcatalog.ui.dialog.simpleerror.SimpleErrorDialogListener
import io.bifri.productcatalog.util.ActivityCreatedObserver
import io.bifri.productcatalog.util.DEFAULT_DIALOG_ID
import io.bifri.productcatalog.util.hideKeyboard
import io.bifri.productcatalog.util.isDialogFragmentShown
import kotlinx.coroutines.channels.BufferOverflow.DROP_OLDEST
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import javax.inject.Inject

const val FRAGMENT_TAG_SIMPLE_ERROR_DIALOG = "simpleErrorDialog"
const val FRAGMENT_TAG_SIMPLE_EDIT_TEXT_DIALOG = "simpleEditTextDialog"
const val DEFAULT_SIMPLE_ERROR_DIALOG_TITLE = R.string.error

abstract class BaseFragment<ARGS : Any, VIEW_STATE> :
    InitFragment(),
    SimpleErrorDialogListener
{
    @Inject lateinit var simpleErrorDialogGetter: SimpleErrorDialogGetter
    private val _initArgsIntent = MutableSharedFlow<ARGS>(
        replay = 1,
        onBufferOverflow = DROP_OLDEST
    )
    protected open val emptyTitle: Boolean = false
    protected open val changeTitle: Boolean = true
    @StringRes open val titleResId: Int = R.string.EMPTY
    protected abstract val args: ARGS
    protected var viewState: VIEW_STATE? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        requireActivity().lifecycle.addObserver(ActivityCreatedObserver { onActivityCreated() })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onInitArgsIntent(args)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(layoutId, container, false)


    override fun onActivityCreated() {
        super.onActivityCreated()
        setTitle(titleResId)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        viewState = null
    }

    override fun onDestroyView() {
        super.onDestroyView()
        requireActivity().currentFocus?.hideKeyboard()
        (parentFragment as? DialogFragment)?.dialog?.currentFocus?.hideKeyboard()
    }

    protected abstract val layoutId: Int

    fun setTitle(@StringRes titleResId: Int) =
        (requireActivity() as AppCompatActivity).supportActionBar!!.setTitle(titleResId)

    fun setTitle(title: CharSequence) {
        (requireActivity() as AppCompatActivity).supportActionBar!!.title = title
    }

    val initArgsIntent: Flow<ARGS> get() = _initArgsIntent.asSharedFlow().distinctUntilChanged()

    private fun onInitArgsIntent(args: ARGS) = _initArgsIntent.tryEmit(args)

    private fun initTitle() {
        if (emptyTitle) {
            setTitle("")
        } else if (changeTitle) {
            setTitle(titleResId)
        }
    }

    protected fun showSimpleErrorDialog(
        dialogId: Int = DEFAULT_DIALOG_ID,
        message: String?,
        title: String = getString(DEFAULT_SIMPLE_ERROR_DIALOG_TITLE)
    ) {
        val dialogTag = simpleErrorDialogFragmentTag(dialogId)
        if (parentFragmentManager.isDialogFragmentShown(dialogTag)) return
        val errorFragmentArgs = ErrorFragmentArgs(dialogId, title, message)
        simpleErrorDialogGetter.newInstance(errorFragmentArgs, this)
            .show(parentFragmentManager, dialogTag)
    }

    override fun onDismissErrorDialog(dialogId: Int) {}

    private fun simpleErrorDialogFragmentTag(id: Int) = FRAGMENT_TAG_SIMPLE_ERROR_DIALOG + id

    private fun simpleEditTextDialogFragmentTag(id: Int) = FRAGMENT_TAG_SIMPLE_EDIT_TEXT_DIALOG + id

}