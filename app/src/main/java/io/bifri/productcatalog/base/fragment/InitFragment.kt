package io.bifri.productcatalog.base.fragment

import android.content.Context
import android.os.Bundle
import android.view.View
import io.bifri.productcatalog.app.App
import io.bifri.productcatalog.base.activity.HasActivityComponent
import io.bifri.productcatalog.base.viewmodel.RxViewBinder

abstract class InitFragment : LifecycleLoggingFragment() {

    abstract val viewBinder: RxViewBinder
    private var isInjectorUsed: Boolean = false

    override fun onAttach(context: Context) {
        inject()
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewBinder.bind(viewLifecycleOwner)
    }

    private fun inject() {
        if (isInjectorUsed) return
        (requireActivity().application as App).fragmentInjector(
            this, (activity as HasActivityComponent).activityComponent!!
        )
            .inject(this)
        isInjectorUsed = true
    }

}