package io.bifri.productcatalog.base.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import io.bifri.productcatalog.exception.ExceptionService
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.flow.SharingStarted.Companion.Lazily

@ExperimentalCoroutinesApi
abstract class BaseViewModel<STATE : Any, PARTIAL_STATE : Any>(
    application: Application,
    private val exceptionService: ExceptionService
) : AndroidViewModel(application) {

    val state: StateFlow<STATE> by lazy { createStateFlow() }

    abstract val allIntentsFlow: Flow<PARTIAL_STATE>

    abstract val initialState: STATE

    abstract fun viewStateReducer(
        previousState: STATE,
        changes: PARTIAL_STATE
    ): STATE

    private fun createStateFlow(): StateFlow<STATE> = allIntentsFlow
        .runningFold(initialState, ::viewStateReducer)
        .drop(1)
        .distinctUntilChanged()
        .catch {
            exceptionService.processException(it)
            throw(it)
        }
        .stateIn(
            scope = viewModelScope,
            started = Lazily,
            initialValue = initialState
        )

}
