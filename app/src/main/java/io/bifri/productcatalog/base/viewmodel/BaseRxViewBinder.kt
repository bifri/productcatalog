package io.bifri.productcatalog.base.viewmodel

import io.bifri.productcatalog.exception.ExceptionService

abstract class BaseRxViewBinder(val exceptionService: ExceptionService) : RxViewBinder()