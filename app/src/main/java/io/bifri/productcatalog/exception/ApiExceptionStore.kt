package io.bifri.productcatalog.exception

import io.bifri.productcatalog.base.store.Store

class ApiExceptionStore : Store<ApiException>(emitRecent = false, distinctUntilChanged = false)