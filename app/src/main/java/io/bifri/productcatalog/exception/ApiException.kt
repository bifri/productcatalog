package io.bifri.productcatalog.exception

class ApiException(val throwable: Throwable)