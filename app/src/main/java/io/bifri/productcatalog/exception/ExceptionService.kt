package io.bifri.productcatalog.exception

interface ExceptionService {
    fun processException(t: Throwable)
}
