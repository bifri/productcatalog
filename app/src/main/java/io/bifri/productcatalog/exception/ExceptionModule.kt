package io.bifri.productcatalog.exception

import androidx.fragment.app.Fragment
import dagger.Module
import dagger.Provides
import io.bifri.productcatalog.di.PerApp
import io.bifri.productcatalog.network.processor.ApiExceptionProcessor
import io.bifri.productcatalog.ui.dialog.simpleerror.ErrorFragmentArgs
import io.bifri.productcatalog.ui.dialog.simpleerror.SimpleErrorDialogFragment
import io.bifri.productcatalog.ui.dialog.simpleerror.SimpleErrorMessageConverter
import io.bifri.productcatalog.util.RxCurrentActivityLifecycleCallbacks


@Module class ExceptionModule {

    @Provides @PerApp fun provideSimpleApiErrorAndExceptionHandler(
            rxCurrentActivityLifecycleCallbacks: RxCurrentActivityLifecycleCallbacks,
            exceptionService: ExceptionService,
            apiExceptionStore: ApiExceptionStore,
            errorConverter: SimpleErrorMessageConverter,
            simpleErrorDialogGetter: SimpleErrorDialogGetter
    ): SimpleApiErrorAndExceptionHandler = SimpleApiErrorAndExceptionHandler(
            rxCurrentActivityLifecycleCallbacks, exceptionService, apiExceptionStore,
            errorConverter, simpleErrorDialogGetter
    )

    @Provides @PerApp fun provideSimpleErrorDialogGetter(): SimpleErrorDialogGetter =
            object : SimpleErrorDialogGetter {
                override fun newInstance(
                        fragmentArgs: ErrorFragmentArgs,
                        targetFragment: Fragment?,
                        requestCode: Int
                ): SimpleErrorDialogFragment =
                        SimpleErrorDialogFragment.newInstance(fragmentArgs, targetFragment, requestCode)
            }

    @Provides @PerApp
    fun provideSimpleErrorMessageConverter(): SimpleErrorMessageConverter =
            SimpleErrorMessageConverter()

    @Provides @PerApp fun provideApiExceptionProcessor(
            exceptionService: ExceptionService,
            apiExceptionStore: ApiExceptionStore
    ): ApiExceptionProcessor = ApiExceptionProcessor(exceptionService, apiExceptionStore)

    @Provides @PerApp fun provideApiExceptionStore(): ApiExceptionStore = ApiExceptionStore()

    @Provides @PerApp fun provideExceptionService(): ExceptionService = DefaultExceptionService()

    @Provides @PerApp fun provideUncaughtExceptionHandler(
            exceptionService: ExceptionService
    ): Thread.UncaughtExceptionHandler =
            UncaughtExceptionHandlerGetter(exceptionService).uncaughtExceptionHandler

}

interface SimpleErrorDialogGetter {
    fun newInstance(
            fragmentArgs: ErrorFragmentArgs = ErrorFragmentArgs(),
            targetFragment: Fragment? = null,
            requestCode: Int = 0
    ): SimpleErrorDialogFragment
}