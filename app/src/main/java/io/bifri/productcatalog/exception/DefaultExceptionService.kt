package io.bifri.productcatalog.exception

import io.bifri.productcatalog.util.loge

class DefaultExceptionService : ExceptionService {

    override fun processException(t: Throwable) {
        loge(message = t.message, throwable = t)
    }

}