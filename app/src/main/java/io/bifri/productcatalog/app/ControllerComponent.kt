package io.bifri.productcatalog.app

import androidx.fragment.app.FragmentActivity
import dagger.BindsInstance
import dagger.Subcomponent
import io.bifri.productcatalog.di.PerController
import io.bifri.productcatalog.ui.main.MainActivityComponent

@PerController @Subcomponent(modules = [ ControllerModule::class ])
interface ControllerComponent {

    @Subcomponent.Builder interface Builder {
        @BindsInstance fun activity(activity: FragmentActivity): Builder

        fun build(): ControllerComponent
    }

    fun newMainActivityComponentBuilder(): MainActivityComponent.Builder

}