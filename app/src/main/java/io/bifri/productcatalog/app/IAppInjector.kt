package io.bifri.productcatalog.app

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import io.bifri.productcatalog.di.Injector

class ComponentInjector(val component: Any, val injector: Injector)

interface IAppInjector {

    fun activityComponentInjector(target: FragmentActivity): ComponentInjector

    fun fragmentInjector(target: Fragment, activityComponent: Any): Injector

}