package io.bifri.productcatalog.app

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import io.bifri.productcatalog.di.ComponentReflectionInjector
import io.bifri.productcatalog.di.Injector
import io.bifri.productcatalog.ui.main.MainActivity
import io.bifri.productcatalog.ui.main.MainActivityComponent
import io.bifri.productcatalog.ui.main.categories.CategoriesFragment
import io.bifri.productcatalog.ui.main.categories.CategoriesFragmentComponent
import io.bifri.productcatalog.ui.main.product.ProductFragment
import io.bifri.productcatalog.ui.main.product.ProductFragmentComponent
import io.bifri.productcatalog.ui.main.products.ProductsFragment
import io.bifri.productcatalog.ui.main.products.ProductsFragmentComponent

class AppInjector(val app: App) : IAppInjector {

    override fun activityComponentInjector(target: FragmentActivity): ComponentInjector {
        val controllerComponent = app.appComponent
                .newControllerComponentBuilder()
                .activity(target)
                .build()
        return when (target::class) {
            MainActivity::class -> {
                val activityComponent = controllerComponent
                        .newMainActivityComponentBuilder()
                        .mainActivity(target as MainActivity)
                        .build()
                ComponentInjector(
                        activityComponent,
                        ComponentReflectionInjector(
                                MainActivityComponent::class.java,
                                activityComponent
                        )
                )
            }
            else -> throw IllegalArgumentException("Unsupported injection target")
        }
    }

    override fun fragmentInjector(target: Fragment, activityComponent: Any): Injector =
            when (target::class) {
                CategoriesFragment::class -> ComponentReflectionInjector(
                        CategoriesFragmentComponent::class.java,
                        (activityComponent as MainActivityComponent)
                                .newCategoriesFragmentComponentBuilder()
                                .categoriesFragment(target as CategoriesFragment)
                                .build()
                )
                ProductsFragment::class -> ComponentReflectionInjector(
                        ProductsFragmentComponent::class.java,
                        (activityComponent as MainActivityComponent)
                                .newProductsFragmentComponentBuilder()
                                .productsFragment(target as ProductsFragment)
                                .build()
                )
                ProductFragment::class -> ComponentReflectionInjector(
                        ProductFragmentComponent::class.java,
                        (activityComponent as MainActivityComponent)
                                .newProductFragmentComponentBuilder()
                                .productFragment(target as ProductFragment)
                                .build()
                )
                else -> throw IllegalArgumentException("Unsupported injection target: ${target::class}")
            }

}