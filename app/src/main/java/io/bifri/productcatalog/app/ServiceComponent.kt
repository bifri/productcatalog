package io.bifri.productcatalog.app

import io.bifri.productcatalog.di.PerService
import dagger.Subcomponent

@PerService @Subcomponent interface ServiceComponent {

    @Subcomponent.Builder interface Builder {
        fun build(): ServiceComponent
    }

//  fun inject(service: MyService)

}