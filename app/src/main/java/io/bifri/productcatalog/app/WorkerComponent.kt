package io.bifri.productcatalog.app

import io.bifri.productcatalog.di.PerWorker
import dagger.Subcomponent

@PerWorker @Subcomponent interface WorkerComponent {

    @Subcomponent.Builder interface Builder {
        fun build(): WorkerComponent
    }

//  fun inject(sendMessagesWorker: SendMessagesWorker)

}