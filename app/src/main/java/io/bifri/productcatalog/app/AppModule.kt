package io.bifri.productcatalog.app

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import io.bifri.productcatalog.di.APP_CONTEXT
import io.bifri.productcatalog.di.APP_INJECTOR
import io.bifri.productcatalog.di.PerApp
import io.bifri.productcatalog.exception.ExceptionModule
import io.bifri.productcatalog.network.NetworkModule
import io.bifri.productcatalog.util.RxCurrentActivityLifecycleCallbacks
import javax.inject.Named

@Module(includes = [
    ExceptionModule::class,
    NetworkModule::class
])
class AppModule {

    @Provides @PerApp fun provideApplication(app: App): Application {
        return app
    }

    @Provides @PerApp @Named(APP_CONTEXT)
    fun provideApplicationContext(application: Application): Context =
            application.applicationContext

    @Provides @PerApp
    fun provideRxCurrentActivityLifecycleCallbacks(app: App): RxCurrentActivityLifecycleCallbacks =
            RxCurrentActivityLifecycleCallbacks(app)

    @Provides @PerApp fun provideObjectMapper() = ObjectMapperGetter().objectMapper

    @Provides @PerApp @Named(APP_INJECTOR) fun provideAppInjector(app: App): IAppInjector {
        return AppInjector(app)
    }

}