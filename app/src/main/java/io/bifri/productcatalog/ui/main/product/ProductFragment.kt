package io.bifri.productcatalog.ui.main.product

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.navigation.fragment.navArgs
import io.bifri.productcatalog.R
import io.bifri.productcatalog.base.fragment.BaseFragment
import io.bifri.productcatalog.exception.ExceptionService
import kotlinx.android.synthetic.main.fragment_product.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import reactivecircus.flowbinding.swiperefreshlayout.refreshes
import javax.inject.Inject


@ExperimentalCoroutinesApi
class ProductFragment : BaseFragment<ProductFragmentArgs, ProductState>() {

    @Inject override lateinit var viewBinder: ProductFragmentVB
    @Inject lateinit var exceptionService: ExceptionService
    override val args by navArgs<ProductFragmentArgs>()
    override val changeTitle: Boolean get() = false
    override val layoutId @LayoutRes get() = R.layout.fragment_product

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSwipeToRefresh()
    }

    override fun onActivityCreated() {
        super.onActivityCreated()
        setTitle(args.productName.orEmpty())
    }

    val refreshIntent: Flow<Unit> get() = swipeRefreshLayoutProduct.refreshes()

    fun render(newViewState: ProductState) {
        setRefreshing(newViewState.isRefreshing)
        fillProduct(newViewState)
        this.viewState = newViewState
    }

    private fun initSwipeToRefresh() {
        swipeRefreshLayoutProduct.setColorSchemeResources(R.color.mainPullToRefreshProgressColor)
    }

    private fun setRefreshing(isRefreshing: Boolean) {
        if (isRefreshing != swipeRefreshLayoutProduct.isRefreshing) {
            swipeRefreshLayoutProduct.isRefreshing = isRefreshing
        }
    }

    private fun fillProduct(newViewState: ProductState) {
        val prevViewState = viewState
        val newProduct = newViewState.product
        if (newProduct != null && newProduct != prevViewState?.product) {
            productViewProduct.setModel(newProduct)
        }
    }

}