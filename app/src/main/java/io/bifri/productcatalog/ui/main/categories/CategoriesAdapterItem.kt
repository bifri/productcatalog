package io.bifri.productcatalog.ui.main.categories

import android.view.View
import io.bifri.productcatalog.R
import io.bifri.productcatalog.util.FlexibleAdapterAlias
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.flexibleadapter.items.IHolder

class CategoriesAdapterItem(
        private val viewHolderGetter: CategoriesAdapterViewHolderGetter,
        val itemModel: CategoriesAdapterItemModel
) : AbstractFlexibleItem<CategoriesAdapterViewHolder>(), IHolder<CategoriesAdapterItemModel> {

    init {
        mSelectable = false
        mDraggable = false
        mSwipeable = false
    }

    override fun createViewHolder(
            view: View,
            adapter: FlexibleAdapterAlias
    ): CategoriesAdapterViewHolder = viewHolderGetter.invoke(view, adapter)

    override fun bindViewHolder(
            adapter: FlexibleAdapterAlias,
            holder: CategoriesAdapterViewHolder,
            position: Int,
            payloads: MutableList<Any>) {
        holder.bind(itemModel)
    }

    override fun getLayoutRes() = R.layout.item_categories_category

    override fun getModel() = itemModel

    override fun shouldNotifyChange(newItem: IFlexible<*>): Boolean {
        return false
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as CategoriesAdapterItem
        if (itemModel != other.itemModel) return false
        return true
    }

    override fun hashCode(): Int = itemModel.hashCode()

}