package io.bifri.productcatalog.ui.main

import dagger.BindsInstance
import dagger.Subcomponent
import io.bifri.productcatalog.ui.main.categories.CategoriesFragmentComponent
import io.bifri.productcatalog.ui.main.product.ProductFragmentComponent
import io.bifri.productcatalog.ui.main.products.ProductsFragmentComponent

@PerMainActivity
@Subcomponent(modules = [
    CommonModule::class,
    MainActivityModule::class
])
interface MainActivityComponent {

    @Subcomponent.Builder interface Builder {
        @BindsInstance fun mainActivity(activity: MainActivity): Builder

        fun build(): MainActivityComponent
    }

    fun newCategoriesFragmentComponentBuilder(): CategoriesFragmentComponent.Builder
    
    fun newProductsFragmentComponentBuilder(): ProductsFragmentComponent.Builder

    fun newProductFragmentComponentBuilder(): ProductFragmentComponent.Builder

    fun inject(mainActivity: MainActivity)

}