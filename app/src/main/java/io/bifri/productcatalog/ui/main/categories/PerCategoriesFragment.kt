package io.bifri.productcatalog.ui.main.categories


import javax.inject.Scope

/**
 * Custom scope for CategoriesFragment singletons
 */
@Scope annotation class PerCategoriesFragment