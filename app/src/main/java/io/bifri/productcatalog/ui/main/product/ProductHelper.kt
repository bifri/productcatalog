package io.bifri.productcatalog.ui.main.product

import androidx.core.text.HtmlCompat
import androidx.core.text.HtmlCompat.FROM_HTML_MODE_LEGACY
import io.bifri.productcatalog.network.response.product.Product
import io.bifri.productcatalog.widgets.ProductViewModel


class ProductHelper {

    fun refreshedProductToProductState(
            previousState: ProductState,
            refreshed: Refreshed
    ): ProductState {
        val product = refreshed.product
        return previousState.copy(
                title = product.shortName,
                product = productViewModel(product),
                isRefreshing = false,
                refreshingError = null
        )
    }

    private fun productViewModel(
            product: Product
    ): ProductViewModel = with(product) {
        ProductViewModel(
                productId,
                primaryImage,
                fullName,
                minSalePrice?.stripTrailingZeros(),
                description?.let { HtmlCompat.fromHtml(it, FROM_HTML_MODE_LEGACY) }
        )
    }

}