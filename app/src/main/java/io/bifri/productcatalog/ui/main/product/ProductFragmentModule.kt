package io.bifri.productcatalog.ui.main.product

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import io.bifri.productcatalog.exception.ExceptionService
import io.bifri.productcatalog.network.processor.SimpleProcessor
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@Module class ProductFragmentModule {

    @Provides @PerProductFragment fun provideProductFragmentVM(
        activity: FragmentActivity,
        fragment: ProductFragment,
        exceptionService: ExceptionService,
        simpleProcessor: SimpleProcessor,
        productHelper: ProductHelper
    ): ProductFragmentVM = ViewModelProvider(
        fragment,
        ProductFragmentVMFactory(
            activity.application,
            exceptionService,
            simpleProcessor,
            productHelper
        )
    )[ProductFragmentVM::class.java]

    @Provides @PerProductFragment fun provideProductFragmentVB(
        view: ProductFragment,
        viewModel: ProductFragmentVM,
        exceptionService: ExceptionService
    ): ProductFragmentVB = ProductFragmentVB(view, viewModel, exceptionService)

    @Provides @PerProductFragment fun provideProductHelper(): ProductHelper =
        ProductHelper()

}