package io.bifri.productcatalog.ui.main.categories

import io.bifri.productcatalog.widgets.CategoryRowModel

data class CategoriesAdapterItemModel(
        val categoryRowModel: CategoryRowModel
)