package io.bifri.productcatalog.ui.main

import io.bifri.productcatalog.base.viewmodel.BaseRxViewBinder
import io.bifri.productcatalog.exception.ExceptionService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class MainActivityVB(
    private val view: MainActivity,
    private val viewModel: MainActivityVM,
    exceptionService: ExceptionService
) : BaseRxViewBinder(exceptionService) {

    override suspend fun bindInternal(coroutineScope: CoroutineScope) {

    }

}