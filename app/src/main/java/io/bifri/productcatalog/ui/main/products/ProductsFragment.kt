package io.bifri.productcatalog.ui.main.products

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import eu.davidea.flexibleadapter.FlexibleAdapter.OnItemClickListener
import eu.davidea.flexibleadapter.items.IFlexible
import io.bifri.productcatalog.R
import io.bifri.productcatalog.app.LinearLayoutManagerGetter
import io.bifri.productcatalog.base.fragment.BaseFragment
import io.bifri.productcatalog.di.VERTICAL_LAYOUT_MANAGER
import io.bifri.productcatalog.exception.ExceptionService
import kotlinx.android.synthetic.main.fragment_products.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.BufferOverflow.DROP_OLDEST
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import reactivecircus.flowbinding.swiperefreshlayout.refreshes
import javax.inject.Inject
import javax.inject.Named


@ExperimentalCoroutinesApi
class ProductsFragment : BaseFragment<ProductsFragmentArgs, ProductsState>() {

    @Inject override lateinit var viewBinder: ProductsFragmentVB
    @Inject lateinit var exceptionService: ExceptionService
    @Inject lateinit var adapterGetter: ProductsAdapterGetter
    @field:[Inject Named(VERTICAL_LAYOUT_MANAGER)]
    lateinit var layoutManagerGetter: LinearLayoutManagerGetter
    @Inject lateinit var adapterItemGetter: ProductsAdapterItemGetter
    private lateinit var adapter: ProductsAdapter
    private val _productClickIntent = MutableSharedFlow<ProductClickArgs>(
        extraBufferCapacity = 1,
        onBufferOverflow = DROP_OLDEST
    )
    override val args by navArgs<ProductsFragmentArgs>()
    override val changeTitle: Boolean get() = false
    override val layoutId @LayoutRes get() = R.layout.fragment_products
    private val adapterOnItemClickListener = OnItemClickListener { _, position ->
        adapter.getItem(position)?.let { item ->
            when (val rawItem = item as IFlexible<*>) {
                is ProductsAdapterItem -> {
                    val productRowModel = rawItem.itemModel.productRowModel
                    productRowModel.url?.let { urlNonNull ->
                        onProductClick(ProductClickArgs(urlNonNull, productRowModel.title))
                    }
                }
                else -> throw IllegalArgumentException()
            }
        }
        true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSwipeToRefresh()
        initAdapter()
        initRecyclerView()
    }

    override fun onActivityCreated() {
        super.onActivityCreated()
        setTitle(args.categoryName.orEmpty())
    }

    override fun onStart() {
        super.onStart()
        adapter.addListener(adapterOnItemClickListener)
    }

    override fun onStop() {
        adapter.removeListener(adapterOnItemClickListener)
        super.onStop()
    }

    val refreshIntent: Flow<Unit> get() = swipeRefreshLayoutProducts.refreshes()

    val productClickIntent: Flow<ProductClickArgs> = _productClickIntent.asSharedFlow()

    private fun onProductClick(args: ProductClickArgs) = _productClickIntent.tryEmit(args)

    fun render(newViewState: ProductsState) {
        setRefreshing(newViewState.isRefreshing)
        fillProducts(newViewState)
        processNavigate(newViewState)
        this.viewState = newViewState
    }

    private fun initSwipeToRefresh() {
        swipeRefreshLayoutProducts.setColorSchemeResources(R.color.mainPullToRefreshProgressColor)
    }

    private fun initAdapter() {
        adapter = adapterGetter.productsAdapter
    }

    private fun initRecyclerView() {
        recyclerViewProducts.apply {
            adapter = this@ProductsFragment.adapter
            layoutManager = layoutManagerGetter.linearLayoutManager
        }
    }

    private fun setRefreshing(isRefreshing: Boolean) {
        if (isRefreshing != swipeRefreshLayoutProducts.isRefreshing) {
            swipeRefreshLayoutProducts.isRefreshing = isRefreshing
        }
    }

    private fun fillProducts(newViewState: ProductsState) {
        val prevViewState = viewState
        val newProducts = newViewState.products
        if (newProducts != null && newProducts != prevViewState?.products) {
            val newItems = newProducts.map {
                @Suppress("UNCHECKED_CAST")
                adapterItemGetter.invoke(it) as IFlexible<ViewHolder>
            }
            adapter.updateDataSet(newItems, false)
        }
    }

    private fun processNavigate(newViewState: ProductsState) {
        newViewState.navigateEvent?.getContentIfNotHandled()?.let {
            when (it) {
                is ProductDirection -> findNavController().navigate(it.navDirections)
            }
        }
    }

}