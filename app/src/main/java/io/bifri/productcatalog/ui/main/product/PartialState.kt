package io.bifri.productcatalog.ui.main.product

import io.bifri.productcatalog.exception.ApiException
import io.bifri.productcatalog.network.processor.Success
import io.bifri.productcatalog.network.processor.Loading
import io.bifri.productcatalog.network.processor.Error
import io.bifri.productcatalog.network.processor.Result
import io.bifri.productcatalog.network.response.product.Product

sealed class PartialState

object Refreshing : PartialState()

data class RefreshingError(val error: ApiException) : PartialState()

data class Refreshed(val product: Product) : PartialState()

val Result<Product>.toProductPartialState get() = when (this) {
    is Loading -> Refreshing
    is Error -> RefreshingError(error)
    is Success -> Refreshed(value)
}