package io.bifri.productcatalog.ui.main.products

import android.view.View
import dagger.Module
import dagger.Provides
import io.bifri.productcatalog.util.FlexibleAdapterAlias

@Module class ProductsRecyclerViewModule {

    @Provides @PerProductsFragment fun provideProductsAdapterGetter(): ProductsAdapterGetter =
            object : ProductsAdapterGetter {
                override val productsAdapter: ProductsAdapter
                    get() = ProductsAdapter(
                            emptyList(),
                            null,
                            true
                    )
            }

    @Provides @PerProductsFragment fun provideProductsAdapterItemGetter(
            viewHolderGetter: ProductsAdapterViewHolderGetter
    ): ProductsAdapterItemGetter = object : ProductsAdapterItemGetter {
        override fun invoke(itemModel: ProductsAdapterItemModel): ProductsAdapterItem =
                ProductsAdapterItem(viewHolderGetter, itemModel)
    }

    @Provides @PerProductsFragment
    fun provideProductsAdapterViewHolderGetter(): ProductsAdapterViewHolderGetter =
            object : ProductsAdapterViewHolderGetter {
                override fun invoke(
                        view: View, adapter: FlexibleAdapterAlias
                ): ProductsAdapterViewHolder = ProductsAdapterViewHolder(
                        view, adapter
                )
            }

}


interface ProductsAdapterGetter {
    val productsAdapter: ProductsAdapter
}

interface ProductsAdapterViewHolderGetter {
    fun invoke(view: View, adapter: FlexibleAdapterAlias): ProductsAdapterViewHolder
}

interface ProductsAdapterItemGetter {
    fun invoke(itemModel: ProductsAdapterItemModel): ProductsAdapterItem
}