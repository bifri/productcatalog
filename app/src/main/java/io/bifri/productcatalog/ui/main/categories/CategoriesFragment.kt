package io.bifri.productcatalog.ui.main.categories

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import eu.davidea.flexibleadapter.FlexibleAdapter.OnItemClickListener
import eu.davidea.flexibleadapter.items.IFlexible
import io.bifri.productcatalog.R
import io.bifri.productcatalog.app.LinearLayoutManagerGetter
import io.bifri.productcatalog.base.fragment.NoArgsBaseFragment
import io.bifri.productcatalog.di.VERTICAL_LAYOUT_MANAGER
import io.bifri.productcatalog.exception.ExceptionService
import kotlinx.android.synthetic.main.fragment_categories.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.BufferOverflow.DROP_OLDEST
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import reactivecircus.flowbinding.swiperefreshlayout.refreshes
import javax.inject.Inject
import javax.inject.Named


@ExperimentalCoroutinesApi
class CategoriesFragment : NoArgsBaseFragment<CategoriesState>() {

    @Inject override lateinit var viewBinder: CategoriesFragmentVB
    @Inject lateinit var exceptionService: ExceptionService
    @Inject lateinit var adapterGetter: CategoriesAdapterGetter
    @field:[Inject Named(VERTICAL_LAYOUT_MANAGER)]
    lateinit var layoutManagerGetter: LinearLayoutManagerGetter
    @Inject lateinit var adapterItemGetter: CategoriesAdapterItemGetter
    private lateinit var adapter: CategoriesAdapter
    private val _categoryClickIntent = MutableSharedFlow<CategoryClickArgs>(
        extraBufferCapacity = 1,
        onBufferOverflow = DROP_OLDEST
    )
    override val titleResId: Int @StringRes get() = R.string.categoriesTitle
    override val layoutId @LayoutRes get() = R.layout.fragment_categories
    private val adapterOnItemClickListener = OnItemClickListener { _, position ->
        adapter.getItem(position)?.let { item ->
            when (val rawItem = item as IFlexible<*>) {
                is CategoriesAdapterItem -> {
                    val categoryRowModel = rawItem.itemModel.categoryRowModel
                    categoryRowModel.url?.let { urlNonNull ->
                        onCategoryClick(CategoryClickArgs(urlNonNull, categoryRowModel.title))
                    }
                }
                else -> throw IllegalArgumentException()
            }
        }
        true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initSwipeToRefresh()
        initAdapter()
        initRecyclerView()
    }

    override fun onStart() {
        super.onStart()
        adapter.addListener(adapterOnItemClickListener)
    }

    override fun onStop() {
        adapter.removeListener(adapterOnItemClickListener)
        super.onStop()
    }

    val refreshIntent: Flow<Unit> get() = swipeRefreshLayoutCategories.refreshes()

    val categoryClickIntent: Flow<CategoryClickArgs> get() = _categoryClickIntent.asSharedFlow()

    private fun onCategoryClick(args: CategoryClickArgs) = _categoryClickIntent.tryEmit(args)

    fun render(newViewState: CategoriesState) {
        setRefreshing(newViewState.isRefreshing)
        fillCategories(newViewState)
        processNavigate(newViewState)
        this.viewState = newViewState
    }

    private fun initSwipeToRefresh() {
        swipeRefreshLayoutCategories.setColorSchemeResources(R.color.mainPullToRefreshProgressColor)
    }

    private fun initAdapter() {
        adapter = adapterGetter.categoriesAdapter
    }

    private fun initRecyclerView() {
        recyclerViewCategories.apply {
            adapter = this@CategoriesFragment.adapter
            layoutManager = layoutManagerGetter.linearLayoutManager
        }
    }

    private fun setRefreshing(isRefreshing: Boolean) {
        if (isRefreshing != swipeRefreshLayoutCategories.isRefreshing) {
            swipeRefreshLayoutCategories.isRefreshing = isRefreshing
        }
    }

    private fun fillCategories(newViewState: CategoriesState) {
        val prevViewState = viewState
        val newCategories = newViewState.categories
        if (newCategories != null
                && newCategories != prevViewState?.categories) {
            val newItems = newCategories.map {
                @Suppress("UNCHECKED_CAST")
                adapterItemGetter.invoke(it) as IFlexible<ViewHolder>
            }
            adapter.updateDataSet(newItems, false)
        }
    }

    private fun processNavigate(newViewState: CategoriesState) {
        newViewState.navigateEvent?.getContentIfNotHandled()?.let {
            when (it) {
                is ProductsDirection -> findNavController().navigate(it.navDirections)
            }
        }
    }

}