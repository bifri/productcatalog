package io.bifri.productcatalog.ui.main.products

import androidx.navigation.NavDirections
import io.bifri.productcatalog.exception.ApiException
import io.bifri.productcatalog.network.processor.Success
import io.bifri.productcatalog.network.processor.Loading
import io.bifri.productcatalog.network.processor.Error
import io.bifri.productcatalog.network.processor.Result
import io.bifri.productcatalog.network.response.products.ProductsResponse

sealed class PartialState

object Refreshing : PartialState()

data class RefreshingError(val error: ApiException) : PartialState()

data class Refreshed(val productsResponse: ProductsResponse) : PartialState()

data class Navigate(val direction: Direction) : PartialState()


sealed class Direction

data class ProductDirection(val navDirections: NavDirections) : Direction()

val Result<ProductsResponse>.toProductsPartialState get() = when (this) {
    is Loading -> Refreshing
    is Error -> RefreshingError(error)
    is Success -> Refreshed(value)
}