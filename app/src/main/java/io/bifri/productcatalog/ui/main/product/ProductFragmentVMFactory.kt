package io.bifri.productcatalog.ui.main.product

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.bifri.productcatalog.exception.ExceptionService
import io.bifri.productcatalog.network.processor.SimpleProcessor
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class ProductFragmentVMFactory(
    private val app: Application,
    private val exceptionService: ExceptionService,
    private val simpleProcessor: SimpleProcessor,
    private val productHelper: ProductHelper
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return ProductFragmentVM(
            app,
            exceptionService,
            simpleProcessor,
            productHelper
        ) as T
    }

}