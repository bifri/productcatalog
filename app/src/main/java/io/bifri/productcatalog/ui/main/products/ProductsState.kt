package io.bifri.productcatalog.ui.main.products

import io.bifri.productcatalog.exception.ApiException
import io.bifri.productcatalog.util.Event

data class ProductsState(
        val products: List<ProductsAdapterItemModel>? = null,
        val isRefreshing: Boolean = false,
        val refreshingError: ApiException? = null,
        val navigateEvent: Event<Direction>? = null
)