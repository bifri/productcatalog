package io.bifri.productcatalog.ui.main

import dagger.Module
import dagger.Provides
import io.bifri.productcatalog.network.productsservice.ProductsApi
import io.bifri.productcatalog.network.processor.ApiExceptionProcessor
import io.bifri.productcatalog.network.processor.SimpleProcessor

@Module class CommonModule {

    @Provides @PerMainActivity fun provideSimpleProcessor(
            apiExceptionProcessor: ApiExceptionProcessor,
            productsApi: ProductsApi
    ): SimpleProcessor = SimpleProcessor(apiExceptionProcessor, productsApi)

}