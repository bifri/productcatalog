package io.bifri.productcatalog.ui.main.categories

import io.bifri.productcatalog.exception.ApiException
import io.bifri.productcatalog.util.Event

data class CategoriesState(
        val categories: List<CategoriesAdapterItemModel>? = null,
        val isRefreshing: Boolean = false,
        val refreshingError: ApiException? = null,
        val navigateEvent: Event<Direction>? = null
)