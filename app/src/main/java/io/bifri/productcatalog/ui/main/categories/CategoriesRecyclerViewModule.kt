package io.bifri.productcatalog.ui.main.categories

import android.view.View
import dagger.Module
import dagger.Provides
import io.bifri.productcatalog.app.StableStringIdGetter
import io.bifri.productcatalog.util.FlexibleAdapterAlias

@Module class CategoriesRecyclerViewModule {

    @Provides @PerCategoriesFragment fun provideCategoriesAdapterGetter(
            stableStringIdGetter: StableStringIdGetter
    ): CategoriesAdapterGetter = object : CategoriesAdapterGetter {
        override val categoriesAdapter: CategoriesAdapter
            get() = CategoriesAdapter(
                    stableStringIdGetter.stableStringIdHelper,
                    emptyList(),
                    null,
                    true
            )
    }

    @Provides @PerCategoriesFragment fun provideCategoriesAdapterItemGetter(
            viewHolderGetter: CategoriesAdapterViewHolderGetter
    ): CategoriesAdapterItemGetter = object : CategoriesAdapterItemGetter {
        override fun invoke(itemModel: CategoriesAdapterItemModel): CategoriesAdapterItem =
                CategoriesAdapterItem(viewHolderGetter, itemModel)
    }

    @Provides @PerCategoriesFragment
    fun provideCategoriesAdapterViewHolderGetter(): CategoriesAdapterViewHolderGetter =
            object : CategoriesAdapterViewHolderGetter {
                override fun invoke(
                        view: View, adapter: FlexibleAdapterAlias
                ): CategoriesAdapterViewHolder = CategoriesAdapterViewHolder(
                        view, adapter
                )
            }

}


interface CategoriesAdapterGetter {
    val categoriesAdapter: CategoriesAdapter
}

interface CategoriesAdapterViewHolderGetter {
    fun invoke(view: View, adapter: FlexibleAdapterAlias): CategoriesAdapterViewHolder
}

interface CategoriesAdapterItemGetter {
    fun invoke(itemModel: CategoriesAdapterItemModel): CategoriesAdapterItem
}