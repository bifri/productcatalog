package io.bifri.productcatalog.ui.main.products

import io.bifri.productcatalog.base.viewmodel.BaseRxViewBinder
import io.bifri.productcatalog.exception.ExceptionService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class ProductsFragmentVB(
    private val view: ProductsFragment,
    private val viewModel: ProductsFragmentVM,
    exceptionService: ExceptionService
) : BaseRxViewBinder(exceptionService) {

    override suspend fun bindInternal(coroutineScope: CoroutineScope) {
        with(coroutineScope) {
            launch { view.initArgsIntent.collect(viewModel::onInitArgsIntent) }
            launch { view.refreshIntent.collect(viewModel::onRefreshIntent) }
            launch { view.productClickIntent.collect(viewModel::onProductClickIntent) }
            launch { viewModel.state.collect(view::render) }
        }
    }

}