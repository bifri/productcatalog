package io.bifri.productcatalog.ui.main.categories

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.bifri.productcatalog.exception.ExceptionService
import io.bifri.productcatalog.network.processor.SimpleProcessor
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class CategoriesFragmentVMFactory(
        private val app: Application,
        private val exceptionService: ExceptionService,
        private val simpleProcessor: SimpleProcessor,
        private val categoriesHelper: CategoriesHelper
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return CategoriesFragmentVM(
                app,
                exceptionService,
                simpleProcessor,
                categoriesHelper
        ) as T
    }

}