package io.bifri.productcatalog.ui.main.products

import android.view.View
import io.bifri.productcatalog.R
import io.bifri.productcatalog.util.FlexibleAdapterAlias
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.flexibleadapter.items.IHolder

class ProductsAdapterItem(
        private val viewHolderGetter: ProductsAdapterViewHolderGetter,
        val itemModel: ProductsAdapterItemModel
) : AbstractFlexibleItem<ProductsAdapterViewHolder>(), IHolder<ProductsAdapterItemModel> {

    init {
        mSelectable = false
        mDraggable = false
        mSwipeable = false
    }

    override fun createViewHolder(
            view: View,
            adapter: FlexibleAdapterAlias
    ): ProductsAdapterViewHolder = viewHolderGetter.invoke(view, adapter)

    override fun bindViewHolder(
            adapter: FlexibleAdapterAlias,
            holder: ProductsAdapterViewHolder,
            position: Int,
            payloads: MutableList<Any>) {
        holder.bind(itemModel)
    }

    override fun getLayoutRes() = R.layout.item_products_product

    override fun getModel() = itemModel

    override fun shouldNotifyChange(newItem: IFlexible<*>): Boolean {
        return false
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as ProductsAdapterItem
        if (itemModel != other.itemModel) return false
        return true
    }

    override fun hashCode(): Int = itemModel.hashCode()

}