package io.bifri.productcatalog.ui.main

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import io.bifri.productcatalog.exception.ExceptionService
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@Module
class MainActivityModule {

    @Provides @PerMainActivity fun provideMainActivityVM(
        activity: FragmentActivity,
        exceptionService: ExceptionService
    ): MainActivityVM = ViewModelProvider(
        activity,
        MainActivityVMFactory(activity.application, exceptionService)
    )[MainActivityVM::class.java]

    @Provides @PerMainActivity fun provideMainActivityVB(
        view: MainActivity,
        viewModel: MainActivityVM,
        exceptionService: ExceptionService
    ): MainActivityVB = MainActivityVB(view, viewModel, exceptionService)

}