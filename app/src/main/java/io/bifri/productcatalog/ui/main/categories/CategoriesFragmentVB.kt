package io.bifri.productcatalog.ui.main.categories

import io.bifri.productcatalog.base.viewmodel.BaseRxViewBinder
import io.bifri.productcatalog.exception.ExceptionService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class CategoriesFragmentVB(
    private val view: CategoriesFragment,
    private val viewModel: CategoriesFragmentVM,
    exceptionService: ExceptionService
) : BaseRxViewBinder(exceptionService) {

    override suspend fun bindInternal(coroutineScope: CoroutineScope) {
        with(coroutineScope) {
            launch { view.refreshIntent.collect(viewModel::onRefreshIntent) }
            launch { view.categoryClickIntent.collect(viewModel::onCategoryClickIntent) }
            launch { viewModel.state.collect(view::render) }
        }
    }

}