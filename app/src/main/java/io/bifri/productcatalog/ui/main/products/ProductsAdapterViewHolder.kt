package io.bifri.productcatalog.ui.main.products

import android.view.View
import eu.davidea.viewholders.FlexibleViewHolder
import io.bifri.productcatalog.util.FlexibleAdapterAlias
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_products_product.*

class ProductsAdapterViewHolder(
        view: View,
        flexibleAdapter: FlexibleAdapterAlias
) : FlexibleViewHolder(view, flexibleAdapter), LayoutContainer {

    override val containerView: View? get() = contentView

    fun bind(item: ProductsAdapterItemModel) {
        productsProductRowProductItem.setModel(item.productRowModel)
    }

}