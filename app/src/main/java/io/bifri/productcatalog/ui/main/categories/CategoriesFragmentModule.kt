package io.bifri.productcatalog.ui.main.categories

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import io.bifri.productcatalog.exception.ExceptionService
import io.bifri.productcatalog.network.processor.SimpleProcessor
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@Module(includes = [ CategoriesRecyclerViewModule::class ])
class CategoriesFragmentModule {

    @Provides @PerCategoriesFragment fun provideCategoriesFragmentVM(
            activity: FragmentActivity,
            fragment: CategoriesFragment,
            exceptionService: ExceptionService,
            simpleProcessor: SimpleProcessor,
            categoriesHelper: CategoriesHelper
    ): CategoriesFragmentVM = ViewModelProvider(
            fragment,
            CategoriesFragmentVMFactory(
                    activity.application,
                    exceptionService,
                    simpleProcessor,
                    categoriesHelper
            )
    )
            .get(CategoriesFragmentVM::class.java)

    @Provides @PerCategoriesFragment fun provideCategoriesFragmentVB(
            view: CategoriesFragment,
            viewModel: CategoriesFragmentVM,
            exceptionService: ExceptionService
    ): CategoriesFragmentVB = CategoriesFragmentVB(view, viewModel, exceptionService)

    @Provides @PerCategoriesFragment fun provideCategoriesHelper(): CategoriesHelper =
            CategoriesHelper()

}