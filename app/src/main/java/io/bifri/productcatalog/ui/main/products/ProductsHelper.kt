package io.bifri.productcatalog.ui.main.products

import io.bifri.productcatalog.network.response.product.sanitizedProducts
import io.bifri.productcatalog.network.response.products.ProductsResponse.GridProducts.Element
import io.bifri.productcatalog.widgets.ProductRowModel


class ProductsHelper {

    fun refreshedProductsToProductsState(
            previousState: ProductsState,
            refreshed: Refreshed
    ): ProductsState {
        val products = refreshed.productsResponse.sanitizedProducts
        return previousState.copy(
                products = converterProductsAdapterItemModels(products),
                isRefreshing = false,
                refreshingError = null
        )
    }

    private fun converterProductsAdapterItemModels(
            products: List<Element>
    ): List<ProductsAdapterItemModel> = products.map { it.toProductAdapterItemModel() }

    private fun Element.toProductAdapterItemModel() =
            ProductsAdapterItemModel(
                    ProductRowModel(
                            productIdNonNull,
                            primaryImage,
                            shortNameNonNull,
                            price?.stripTrailingZeros(),
                            url
                    )
            )

}