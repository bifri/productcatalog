package io.bifri.productcatalog.ui.main

import android.app.Application
import io.bifri.productcatalog.base.viewmodel.BaseViewModel
import io.bifri.productcatalog.exception.ExceptionService
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emptyFlow


@ExperimentalCoroutinesApi
class MainActivityVM(
    application: Application,
    exceptionService: ExceptionService
) : BaseViewModel<Any, Any>(application, exceptionService) {

    override val allIntentsFlow: Flow<Any> get() = emptyFlow()
    override val initialState get() = Any()

    override fun viewStateReducer(previousState: Any, changes: Any) = previousState

}