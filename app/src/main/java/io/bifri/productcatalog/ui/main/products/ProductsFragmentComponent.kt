package io.bifri.productcatalog.ui.main.products

import dagger.BindsInstance
import dagger.Subcomponent
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@PerProductsFragment
@Subcomponent(modules = [ ProductsFragmentModule::class ])
interface ProductsFragmentComponent {

    @Subcomponent.Builder interface Builder {
        @BindsInstance fun productsFragment(fragment: ProductsFragment): Builder

        fun build(): ProductsFragmentComponent
    }

    fun inject(productsFragment: ProductsFragment)

}
