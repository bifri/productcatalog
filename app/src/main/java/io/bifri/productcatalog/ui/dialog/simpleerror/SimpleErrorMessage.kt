package io.bifri.productcatalog.ui.dialog.simpleerror

import io.bifri.productcatalog.network.productsservice.RetrofitException
import io.bifri.productcatalog.network.response.ErrorResponse

private const val MAX_MESSAGE_LENGTH = 256

data class SimpleErrorMessage(val message: String?)

class SimpleErrorMessageConverter {

    fun from(serverError: String): SimpleErrorMessage = SimpleErrorMessage(
            serverError.take(MAX_MESSAGE_LENGTH)
    )

    fun from(throwable: Throwable): SimpleErrorMessage {
        var message: String? = null
        if (throwable is RetrofitException && throwable.kind == RetrofitException.Kind.HTTP) {
            val errorResponse = throwable.errorBodyAs(ErrorResponse::class.java)
            message = errorResponse?.message ?: errorResponse?.status
        }
        if (message == null) message = throwable.message
        message = message?.take(MAX_MESSAGE_LENGTH).orEmpty()
        return SimpleErrorMessage(message)
    }

}