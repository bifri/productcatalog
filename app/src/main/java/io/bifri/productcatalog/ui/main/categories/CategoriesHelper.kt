package io.bifri.productcatalog.ui.main.categories

import io.bifri.productcatalog.network.response.categories.Category
import io.bifri.productcatalog.network.response.categories.sanitized
import io.bifri.productcatalog.widgets.CategoryRowModel


class CategoriesHelper {

    fun refreshedCategoriesToCategoriesState(
            previousState: CategoriesState,
            refreshed: Refreshed
    ): CategoriesState {
        val categories = refreshed.categories.sanitized
        return previousState.copy(
                categories = converterCategoriesAdapterItemModels(categories),
                isRefreshing = false,
                refreshingError = null
        )
    }

    private fun converterCategoriesAdapterItemModels(
            categories: List<Category>
    ): List<CategoriesAdapterItemModel> = categories.map { it.toCategoryAdapterItemModel() }

    private fun Category.toCategoryAdapterItemModel() =
            CategoriesAdapterItemModel(CategoryRowModel(categoryIdNonNull, shortNameNonNull, url))

}