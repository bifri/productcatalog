package io.bifri.productcatalog.ui.main.product

import io.bifri.productcatalog.exception.ApiException
import io.bifri.productcatalog.widgets.ProductViewModel

data class ProductState(
        val title: String? = null,
        val product: ProductViewModel? = null,
        val isRefreshing: Boolean = false,
        val refreshingError: ApiException? = null
)