package io.bifri.productcatalog.ui.main.categories

import androidx.navigation.NavDirections
import io.bifri.productcatalog.exception.ApiException
import io.bifri.productcatalog.network.processor.Success
import io.bifri.productcatalog.network.processor.Loading
import io.bifri.productcatalog.network.processor.Error
import io.bifri.productcatalog.network.processor.Result
import io.bifri.productcatalog.network.response.categories.Category

sealed class PartialState

object Refreshing : PartialState()

data class RefreshingError(val error: ApiException) : PartialState()

data class Refreshed(val categories: List<Category?>) : PartialState()

data class Navigate(val direction: Direction) : PartialState()


sealed class Direction

data class ProductsDirection(val navDirections: NavDirections) : Direction()


val Result<List<Category?>>.toCategoriesPartialState get() = when (this) {
    is Loading -> Refreshing
    is Error -> RefreshingError(error)
    is Success -> Refreshed(value)
}