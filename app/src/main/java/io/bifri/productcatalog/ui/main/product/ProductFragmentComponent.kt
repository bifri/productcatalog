package io.bifri.productcatalog.ui.main.product

import dagger.BindsInstance
import dagger.Subcomponent

@PerProductFragment
@Subcomponent(modules = [ ProductFragmentModule::class ])
interface ProductFragmentComponent {

    @Subcomponent.Builder interface Builder {
        @BindsInstance fun productFragment(fragment: ProductFragment): Builder

        fun build(): ProductFragmentComponent
    }

    fun inject(productFragment: ProductFragment)

}
