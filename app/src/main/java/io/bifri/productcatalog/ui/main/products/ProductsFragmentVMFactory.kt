package io.bifri.productcatalog.ui.main.products

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.bifri.productcatalog.exception.ExceptionService
import io.bifri.productcatalog.network.processor.SimpleProcessor
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class ProductsFragmentVMFactory(
    private val app: Application,
    private val exceptionService: ExceptionService,
    private val simpleProcessor: SimpleProcessor,
    private val productsHelper: ProductsHelper
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return ProductsFragmentVM(
            app,
            exceptionService,
            simpleProcessor,
            productsHelper
        ) as T
    }

}