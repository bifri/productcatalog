package io.bifri.productcatalog.ui.main.categories

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import eu.davidea.flexibleadapter.items.IFlexible
import io.bifri.productcatalog.common.adapter.StableStringIdAdapter
import io.bifri.productcatalog.common.adapter.StableStringIdHelper


class CategoriesAdapter(
        stableStringIdHelper: StableStringIdHelper,
        items: List<IFlexible<ViewHolder>>?,
        listeners: Any?,
        stableIds: Boolean
) : StableStringIdAdapter<IFlexible<ViewHolder>, ViewHolder>(
        stableStringIdHelper, items, listeners, stableIds
) {

    init {
        setNotifyChangeOfUnfilteredItems(false)
    }

    override fun getItemId(position: Int): Long = (getItem(position) as IFlexible<*>?)?.let {
        when (it) {
            is CategoriesAdapterItem -> stableStringIdHelper.longId(
                    it.itemModel.categoryRowModel.title
            )
            else -> throw IllegalArgumentException()
        }
    }
            ?: RecyclerView.NO_ID
}