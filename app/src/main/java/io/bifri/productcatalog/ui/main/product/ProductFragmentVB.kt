package io.bifri.productcatalog.ui.main.product

import io.bifri.productcatalog.base.viewmodel.BaseRxViewBinder
import io.bifri.productcatalog.exception.ExceptionService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class ProductFragmentVB(
    private val view: ProductFragment,
    private val viewModel: ProductFragmentVM,
    exceptionService: ExceptionService
) : BaseRxViewBinder(exceptionService) {

    override suspend fun bindInternal(coroutineScope: CoroutineScope) {
        with(coroutineScope) {
            launch { view.initArgsIntent.collect(viewModel::onInitArgsIntent) }
            launch { view.refreshIntent.collect(viewModel::onRefreshIntent) }
            launch { viewModel.state.collect(view::render) }
        }
    }

}