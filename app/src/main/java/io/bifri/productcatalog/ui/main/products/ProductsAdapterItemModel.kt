package io.bifri.productcatalog.ui.main.products

import io.bifri.productcatalog.widgets.ProductRowModel

data class ProductsAdapterItemModel(
        val productRowModel: ProductRowModel
)