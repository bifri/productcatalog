package io.bifri.productcatalog.ui.main.categories

import dagger.BindsInstance
import dagger.Subcomponent
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@PerCategoriesFragment
@Subcomponent(modules = [ CategoriesFragmentModule::class ])
interface CategoriesFragmentComponent {

    @Subcomponent.Builder interface Builder {
        @BindsInstance fun categoriesFragment(fragment: CategoriesFragment): Builder

        fun build(): CategoriesFragmentComponent
    }

    fun inject(categoriesFragment: CategoriesFragment)

}
