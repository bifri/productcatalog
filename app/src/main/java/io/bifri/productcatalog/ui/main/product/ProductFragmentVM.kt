package io.bifri.productcatalog.ui.main.product

import android.app.Application
import io.bifri.productcatalog.base.viewmodel.BaseViewModel
import io.bifri.productcatalog.exception.ExceptionService
import io.bifri.productcatalog.network.processor.SimpleProcessor
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.BufferOverflow.DROP_OLDEST
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
class ProductFragmentVM(
    application: Application,
    exceptionService: ExceptionService,
    private val simpleProcessor: SimpleProcessor,
    private val productHelper: ProductHelper
) : BaseViewModel<ProductState, PartialState>(application, exceptionService) {

    private val onInitArgsIntent = MutableSharedFlow<ProductFragmentArgs>(
        replay = 1,
        onBufferOverflow = DROP_OLDEST
    )
    private val onRefreshIntent = MutableSharedFlow<Unit>(
        extraBufferCapacity = 1,
        onBufferOverflow = DROP_OLDEST
    )

    fun onInitArgsIntent(args: ProductFragmentArgs) = onInitArgsIntent.tryEmit(args)

    fun onRefreshIntent(unit: Unit) = onRefreshIntent.tryEmit(unit)

    override val allIntentsFlow: Flow<PartialState> get() =
        onInitArgsIntent.flatMapLatest { args ->
            onRefreshIntent
                .onStart { emit(Unit) }
                .flatMapLatest {
                    simpleProcessor.load { product(args.productId) }
                        .map { it.toProductPartialState }
                }
        }

    override val initialState get() = ProductState()

    override fun viewStateReducer(
        previousState: ProductState,
        changes: PartialState
    ): ProductState = when (changes) {
        is Refreshing -> previousState.copy(
            isRefreshing = true,
            refreshingError = null
        )
        is RefreshingError -> previousState.copy(
            isRefreshing = false,
            refreshingError = changes.error
        )
        is Refreshed -> productHelper.refreshedProductToProductState(
            previousState, changes
        )
    }

}