package io.bifri.productcatalog.ui.main.categories

import android.view.View
import eu.davidea.viewholders.FlexibleViewHolder
import io.bifri.productcatalog.util.FlexibleAdapterAlias
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_categories_category.*

class CategoriesAdapterViewHolder(
        view: View,
        flexibleAdapter: FlexibleAdapterAlias
) : FlexibleViewHolder(view, flexibleAdapter), LayoutContainer {

    override val containerView: View? get() = contentView

    fun bind(item: CategoriesAdapterItemModel) {
        categoriesCategoryRowCategoryItem.setModel(item.categoryRowModel)
    }

}