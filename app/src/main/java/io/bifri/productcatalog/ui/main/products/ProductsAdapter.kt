package io.bifri.productcatalog.ui.main.products

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import eu.davidea.flexibleadapter.items.IFlexible
import io.bifri.productcatalog.widgets.FixedFlexibleAdapter


class ProductsAdapter(
        items: List<IFlexible<ViewHolder>>?,
        listeners: Any?,
        stableIds: Boolean
) : FixedFlexibleAdapter<IFlexible<ViewHolder>, ViewHolder>(
        items, listeners, stableIds
) {

    init {
        setNotifyChangeOfUnfilteredItems(false)
    }

    override fun getItemId(position: Int): Long = (getItem(position) as IFlexible<*>?)?.let {
        when (it) {
            is ProductsAdapterItem -> it.itemModel.productRowModel.productId
            else -> throw IllegalArgumentException()
        }
    }
            ?: RecyclerView.NO_ID
}