package io.bifri.productcatalog.ui.main.products


import javax.inject.Scope

/**
 * Custom scope for ProductsFragment singletons
 */
@Scope annotation class PerProductsFragment