package io.bifri.productcatalog.ui.main.products

import android.app.Application
import io.bifri.productcatalog.base.viewmodel.BaseViewModel
import io.bifri.productcatalog.exception.ExceptionService
import io.bifri.productcatalog.network.processor.SimpleProcessor
import io.bifri.productcatalog.util.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.BufferOverflow.DROP_OLDEST
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
class ProductsFragmentVM(
    application: Application,
    exceptionService: ExceptionService,
    private val simpleProcessor: SimpleProcessor,
    private val productsHelper: ProductsHelper
) : BaseViewModel<ProductsState, PartialState>(application, exceptionService) {

    private val onInitArgsIntent = MutableSharedFlow<ProductsFragmentArgs>(
        replay = 1,
        onBufferOverflow = DROP_OLDEST
    )
    private val onRefreshIntent = MutableSharedFlow<Unit>(
        extraBufferCapacity = 1,
        onBufferOverflow = DROP_OLDEST
    )
    private val onProductClickIntent = MutableSharedFlow<ProductClickArgs>(
        extraBufferCapacity = 1,
        onBufferOverflow = DROP_OLDEST
    )

    fun onInitArgsIntent(args: ProductsFragmentArgs) = onInitArgsIntent.tryEmit(args)

    fun onRefreshIntent(unit: Unit) = onRefreshIntent.tryEmit(unit)

    fun onProductClickIntent(args: ProductClickArgs) = onProductClickIntent.tryEmit(args)

    override val allIntentsFlow: Flow<PartialState> get() {
        val products = onInitArgsIntent.flatMapLatest { args ->
            onRefreshIntent
                .onStart { emit(Unit) }
                .flatMapLatest {
                    simpleProcessor.load { products(args.categoryId) }
                        .map { it.toProductsPartialState }
                }
        }

        val navigateToProduct = onProductClickIntent.map {
            val navDirections = ProductsFragmentDirections.actionProductsFragmentToProductFragment(
                it.productId, it.productName
            )
            Navigate(ProductDirection(navDirections))
        }

        return merge(products, navigateToProduct)
    }

    override val initialState get() = ProductsState()

    override fun viewStateReducer(
        previousState: ProductsState,
        changes: PartialState
    ): ProductsState = when (changes) {
        is Refreshing -> previousState.copy(
            isRefreshing = true,
            refreshingError = null
        )
        is RefreshingError -> previousState.copy(
            isRefreshing = false,
            refreshingError = changes.error
        )
        is Refreshed -> productsHelper.refreshedProductsToProductsState(
            previousState, changes
        )
        is Navigate -> previousState.copy(navigateEvent = Event(changes.direction))
    }

}

data class ProductClickArgs(
    val productId: String,
    val productName: String?
)