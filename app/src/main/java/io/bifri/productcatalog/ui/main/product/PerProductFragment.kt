package io.bifri.productcatalog.ui.main.product


import javax.inject.Scope

/**
 * Custom scope for ProductFragment singletons
 */
@Scope annotation class PerProductFragment