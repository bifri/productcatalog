package io.bifri.productcatalog.ui.main.categories

import android.app.Application
import io.bifri.productcatalog.base.viewmodel.BaseViewModel
import io.bifri.productcatalog.exception.ExceptionService
import io.bifri.productcatalog.network.processor.SimpleProcessor
import io.bifri.productcatalog.util.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.BufferOverflow.DROP_OLDEST
import kotlinx.coroutines.flow.*

@ExperimentalCoroutinesApi
class CategoriesFragmentVM(
    application: Application,
    exceptionService: ExceptionService,
    private val simpleProcessor: SimpleProcessor,
    private val categoriesHelper: CategoriesHelper
) : BaseViewModel<CategoriesState, PartialState>(application, exceptionService) {

    private val onRefreshIntent = MutableSharedFlow<Unit>(
        extraBufferCapacity = 1,
        onBufferOverflow = DROP_OLDEST
    )
    private val onCategoryClickIntent = MutableSharedFlow<CategoryClickArgs>(
        extraBufferCapacity = 1,
        onBufferOverflow = DROP_OLDEST
    )

    fun onRefreshIntent(unit: Unit) = onRefreshIntent.tryEmit(unit)

    fun onCategoryClickIntent(args: CategoryClickArgs) =
        onCategoryClickIntent.tryEmit(args)

    override val allIntentsFlow: Flow<PartialState> get() {
        val categories = onRefreshIntent
            .onStart { emit(Unit) }
            .flatMapLatest {
                simpleProcessor.load { categories() }
                    .map { it.toCategoriesPartialState }
            }

        val navigateToProducts = onCategoryClickIntent.map {
            val navDirections = CategoriesFragmentDirections.actionCategoriesFragmentToProductsFragment(
                it.categoryId, it.categoryName
            )
            Navigate(ProductsDirection(navDirections))
        }

        return merge(categories, navigateToProducts)
    }

    override val initialState get() = CategoriesState()

    override fun viewStateReducer(
        previousState: CategoriesState,
        changes: PartialState
    ): CategoriesState = when (changes) {
        is Refreshing -> previousState.copy(
            isRefreshing = true,
            refreshingError = null
        )
        is RefreshingError -> previousState.copy(
            isRefreshing = false,
            refreshingError = changes.error
        )
        is Refreshed -> categoriesHelper.refreshedCategoriesToCategoriesState(
            previousState, changes
        )
        is Navigate -> previousState.copy(navigateEvent = Event(changes.direction))
    }

}

data class CategoryClickArgs(
    val categoryId: String,
    val categoryName: String?
)