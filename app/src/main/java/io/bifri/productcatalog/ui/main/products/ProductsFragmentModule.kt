package io.bifri.productcatalog.ui.main.products

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import io.bifri.productcatalog.exception.ExceptionService
import io.bifri.productcatalog.network.processor.SimpleProcessor
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@Module(includes = [ ProductsRecyclerViewModule::class ])
class ProductsFragmentModule {

    @Provides @PerProductsFragment fun provideProductsFragmentVM(
        activity: FragmentActivity,
        fragment: ProductsFragment,
        exceptionService: ExceptionService,
        simpleProcessor: SimpleProcessor,
        productsHelper: ProductsHelper
    ): ProductsFragmentVM = ViewModelProvider(
        fragment,
        ProductsFragmentVMFactory(
            activity.application,
            exceptionService,
            simpleProcessor,
            productsHelper
        )
    )[ProductsFragmentVM::class.java]

    @Provides @PerProductsFragment fun provideProductsFragmentVB(
        view: ProductsFragment,
        viewModel: ProductsFragmentVM,
        exceptionService: ExceptionService
    ): ProductsFragmentVB = ProductsFragmentVB(view, viewModel, exceptionService)

    @Provides @PerProductsFragment fun provideProductsHelper(): ProductsHelper =
        ProductsHelper()

}