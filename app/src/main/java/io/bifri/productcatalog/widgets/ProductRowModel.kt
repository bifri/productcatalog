package io.bifri.productcatalog.widgets

import java.math.BigDecimal


data class ProductRowModel(
        val productId: Long,
        val image: String?,
        val title: String,
        val price: BigDecimal?,
        val url: String?
)