package io.bifri.productcatalog.widgets

import androidx.cardview.widget.CardView
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import io.bifri.productcatalog.R
import io.bifri.productcatalog.glide.GlideApp
import io.bifri.productcatalog.util.setTextIfChanged
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_product.*


private const val BASE_IMAGE_URL =
        "http://images1.opticsplanet.com/365-240-ffffff/%s.jpg"

class ProductViewHelper(
        override val containerView: CardView
) : LayoutContainer {

    private var model: ProductViewModel? = null

    init {
        with(containerView) {
            radius = context.resources.getDimension(R.dimen.productProductViewCornerRadius)
        }
    }

    fun setModel(item: ProductViewModel) {
        if (model == item) return

        with(item) {
            if (!image.isNullOrEmpty()) {
                GlideApp
                        .with(imageViewProduct)
                        .load(BASE_IMAGE_URL.format(image))
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(imageViewProduct)
            } else {
                GlideApp
                        .with(imageViewProduct)
                        .clear(imageViewProduct)
                imageViewProduct.setImageDrawable(null)
            }

            textViewProductName.setTextIfChanged(name)
            textViewProductDescription.setTextIfChanged(description)
            textViewProductPrice.setTextIfChanged(price?.toPlainString())

        }

        model = item
    }

}