package io.bifri.productcatalog.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.annotation.AttrRes
import io.bifri.productcatalog.R

class CategoryRow : FrameLayout {

    private val helper: CategoryRowHelper

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, @AttrRes defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    init {
        LayoutInflater.from(context).inflate(R.layout.view_categories_category_row, this, true)
        helper = CategoryRowHelper(this)
    }

    val setModel = helper::setModel

}