package io.bifri.productcatalog.widgets

import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import io.bifri.productcatalog.glide.GlideApp
import io.bifri.productcatalog.util.setTextIfChanged
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_products_product_row.*

private const val BASE_IMAGE_URL =
        "http://images1.opticsplanet.com/120-90-ffffff/%s.jpg"

class ProductRowHelper(
        override val containerView: ProductRow
) : LayoutContainer {

    private var model: ProductRowModel? = null

    fun setModel(item: ProductRowModel) {
        if (model == item) return

        with(item) {
            if (!image.isNullOrEmpty()) {
                GlideApp
                        .with(imageViewProductsProductRow)
                        .load(BASE_IMAGE_URL.format(image))
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .into(imageViewProductsProductRow)
            } else {
                GlideApp
                        .with(imageViewProductsProductRow)
                        .clear(imageViewProductsProductRow)
                imageViewProductsProductRow.setImageDrawable(null)
            }

            textViewProductsProductRowTitle.setTextIfChanged(title)
            textViewProductsProductRowPrice.setTextIfChanged(price?.toPlainString())
        }

        model = item
    }

}