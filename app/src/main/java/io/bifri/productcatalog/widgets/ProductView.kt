package io.bifri.productcatalog.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.annotation.AttrRes
import androidx.cardview.widget.CardView
import io.bifri.productcatalog.R


class ProductView : CardView {

    private val productViewHelper: ProductViewHelper

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, @AttrRes defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    init {
        LayoutInflater.from(context).inflate(
                R.layout.view_product,
                this,
                true
        )
        productViewHelper = ProductViewHelper(this)
    }

    val setModel = productViewHelper::setModel

}