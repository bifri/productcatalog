package io.bifri.productcatalog.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.annotation.AttrRes
import androidx.constraintlayout.widget.ConstraintLayout
import io.bifri.productcatalog.R

class ProductRow : ConstraintLayout {

    private val helper: ProductRowHelper

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, @AttrRes defStyleAttr: Int) :
            super(context, attrs, defStyleAttr)

    init {
        LayoutInflater.from(context).inflate(R.layout.view_products_product_row, this, true)
        helper = ProductRowHelper(this)
    }

    val setModel = helper::setModel

}