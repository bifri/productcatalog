package io.bifri.productcatalog.widgets

import java.math.BigDecimal

data class ProductViewModel(
        val id: Long?,
        val image: String?,
        val name: String?,
        val price: BigDecimal?,
        val description: CharSequence?
)