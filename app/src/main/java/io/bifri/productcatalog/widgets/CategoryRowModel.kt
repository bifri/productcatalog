package io.bifri.productcatalog.widgets


data class CategoryRowModel(
        val categoryId: String,
        val title: String,
        val url: String?
)