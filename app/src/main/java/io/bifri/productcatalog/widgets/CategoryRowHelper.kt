package io.bifri.productcatalog.widgets

import io.bifri.productcatalog.util.setTextIfChanged
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_categories_category_row.*


class CategoryRowHelper(
        override val containerView: CategoryRow
) : LayoutContainer {

    private var model: CategoryRowModel? = null

    fun setModel(item: CategoryRowModel) {
        if (model == item) return

        with(item) {
            textViewCategoriesCategoryRowTitle.setTextIfChanged(title)
        }

        model = item
    }

}