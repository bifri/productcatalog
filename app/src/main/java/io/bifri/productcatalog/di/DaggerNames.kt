package io.bifri.productcatalog.di

const val APP_CONTEXT = "appContext"
const val ACTIVITY_CONTEXT = "activityContext"
const val APP_INJECTOR = "appInjector"
const val API_KEY_INTERCEPTOR = "apiKeyInterceptor"
const val LOGGING_INTERCEPTOR = "loggingInterceptor"
const val VERTICAL_LAYOUT_MANAGER = "verticalLayoutManager"