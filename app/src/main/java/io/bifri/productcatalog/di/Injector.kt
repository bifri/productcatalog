package io.bifri.productcatalog.di

interface Injector {
    fun inject(target: Any)
}