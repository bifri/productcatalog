package io.bifri.productcatalog.di


import javax.inject.Scope

/**
 * Custom scope for Android Service singletons
 */
@Scope annotation class PerService