package io.bifri.productcatalog.di


import javax.inject.Scope

/**
 * Custom scope for Android Workers singletons
 */
@Scope annotation class PerWorker