package io.bifri.productcatalog.ui.main.categories


import android.content.Intent
import android.widget.TextView
import androidx.navigation.fragment.NavHostFragment
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.internal.runner.junit4.statement.UiThreadStatement
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.robotium.solo.Solo
import io.bifri.productcatalog.R
import io.bifri.productcatalog.ui.main.MainActivity
import io.bifri.productcatalog.widgets.CategoryRowModel
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


private const val CREATE_ACTIVITY_WAIT_TIME = 4_000
private const val RENDER_WAIT_TIME = 4_000

@RunWith(AndroidJUnit4::class)
class CategoriesFragmentTest {

    @Rule @JvmField val activityTestRule: ActivityTestRule<MainActivity> =
            ActivityTestRule(MainActivity::class.java, false, false)
    private lateinit var solo: Solo

    @Test fun categoriesFragmentRenderTest() {
        val instrumentation = InstrumentationRegistry.getInstrumentation()
        val intent = Intent()
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val mainActivity: MainActivity = activityTestRule.launchActivity(intent)
        val soloConfig = Solo.Config()
        solo = Solo(instrumentation, soloConfig, mainActivity)
        solo.waitForActivity(MainActivity::class.java, CREATE_ACTIVITY_WAIT_TIME)

        val categoriesFragment = mainActivity
                .supportFragmentManager.fragments.filterIsInstance<NavHostFragment>()[0]
                .childFragmentManager.fragments.filterIsInstance<CategoriesFragment>()[0]

        UiThreadStatement.runOnUiThread {
            categoriesFragment.render(categoriesState)
        }
        solo.sleep(RENDER_WAIT_TIME)
        checkCategories()

        UiThreadStatement.runOnUiThread {
            categoriesFragment.render(refreshingState)
        }
        solo.sleep(RENDER_WAIT_TIME)
        checkRefreshing()
    }

    private fun checkCategories() {
        val categoryTitles = solo.getCurrentViews(TextView::class.java)
                .filter { it.id == R.id.textViewCategoriesCategoryRowTitle }
                .map { it.text }
        val correctCategoryTitles = listOf("Title1", "Title2", "Title3")
        assertEquals(correctCategoryTitles, categoryTitles)
    }

    private fun checkRefreshing() {
        val swipeRefreshLayout = solo.getView(
                R.id.swipeRefreshLayoutCategories
        ) as SwipeRefreshLayout
        assertEquals(true, swipeRefreshLayout.isRefreshing)
    }

    private val categoriesState get() =
        CategoriesState(
                categories = listOf(
                        CategoriesAdapterItemModel(
                                CategoryRowModel(
                                        "1",
                                        "Title1",
                                        "url1"
                                )
                        ),
                        CategoriesAdapterItemModel(
                                CategoryRowModel(
                                        "2",
                                        "Title2",
                                        "url2"
                                )
                        ),
                        CategoriesAdapterItemModel(
                                CategoryRowModel(
                                        "3",
                                        "Title3",
                                        "url3"
                                )
                        )
                )
        )

    private val refreshingState get() =
        categoriesState.copy(isRefreshing = true)

}