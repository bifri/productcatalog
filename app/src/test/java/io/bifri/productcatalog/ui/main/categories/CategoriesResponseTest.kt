package io.bifri.productcatalog.ui.main.categories

import io.bifri.productcatalog.network.response.categories.Category
import io.bifri.productcatalog.network.response.categories.sanitized
import org.junit.Assert
import org.junit.Test

class CategoriesResponseTest {

    @Test fun categoriesResponseTest() {
        val category1 = Category(
                "1",
                "0",
                "ATV Box Accessories",
                "99",
                "99",
                "99.999",
                "ATV Box Accessories",
                "99",
                "atv-boxes-accessories"
        )
        val category2 = Category(
                "2",
                "0",
                "ATV Box Accessories",
                "99",
                "99",
                "99.999",
                "ATV Box Accessories",
                "99",
                "atv-boxes-accessories"
        )

        val category3 = Category(
                null,
                "0",
                "ATV Box Accessories",
                "99",
                "99",
                "99.999",
                "ATV Box Accessories",
                "99",
                "atv-boxes-accessories"
        )

        val categoriesResponse = listOf(category1, category2, category3)
        val sanitizedCategories = listOf(category1, category2)
        Assert.assertEquals(sanitizedCategories, categoriesResponse.sanitized)
    }

}